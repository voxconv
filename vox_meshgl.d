module vox_meshgl;

import iv.bclamp;
import iv.glbinds.utils;
import iv.cmdcongl;
import iv.strex;
import iv.vfs;
import iv.vfs.io;
import iv.vmath;

static import iv.timer;

import vox_3dbmp;
import vox_mesh;


// ////////////////////////////////////////////////////////////////////////// //
public __gshared bool vox_fix_tjunctions = false;
public __gshared bool vox_wireframe = false;


// ////////////////////////////////////////////////////////////////////////// //
enum BreakIndex = 6553500;


// ////////////////////////////////////////////////////////////////////////// //
/*
  this builds the OpenGL data structures, ready to be uploaded to the GPU.
  it also can perform t-junction fixing. it is using quad data from `VoxelMesh`
  as a source, and creates triangle fans from those, calucluating the proper
  texture mapping coordinates.
 */
struct GLVoxelMesh {
  static align(1) struct VVoxVertexEx {
  align(1):
    float x, y, z;
    float s, t;
    float nx, ny, nz; // normal

    float get (uint idx) const nothrow @safe @nogc {
      pragma(inline, true);
      assert(idx < 3);
      return (idx == 0 ? x : idx == 1 ? y : z);
    }

    void set (uint idx, const float v) nothrow @safe @nogc {
      pragma(inline, true);
      assert(idx < 3);
           if (idx == 0) x = v;
      else if (idx == 1) y = v;
      else z = v;
    }
  }

  VVoxVertexEx[] vertices;
  uint[] indicies;
  uint[] lindicies; // lines
  uint uniqueVerts;

  bool sealed;
  uint[VVoxVertexEx] vertcache;
  float[3] vmin; // minimum vertex coords
  float[3] vmax; // maximum vertex coords

  uint[] img;
  uint imgWidth, imgHeight;

public:
  void save (VFile fl) {
    fl.rawWriteExact("K8VOXGL0");

    fl.writeNum!uint(cast(uint)BreakIndex);

    // texture
    fl.writeNum!ushort(cast(ushort)imgWidth);
    fl.writeNum!ushort(cast(ushort)imgHeight);
    fl.rawWriteExact(img[0..imgWidth*imgHeight]);

    // vertices
    fl.writeNum!uint(cast(uint)vertices.length);
    for (uint f = 0; f < cast(uint)vertices.length; ++f) {
      fl.writeNum(vertices[f].x);
      fl.writeNum(vertices[f].y);
      fl.writeNum(vertices[f].z);
      fl.writeNum(vertices[f].s);
      fl.writeNum(vertices[f].t);
      fl.writeNum(vertices[f].nx);
      fl.writeNum(vertices[f].ny);
      fl.writeNum(vertices[f].nz);
    }

    // indicies
    fl.writeNum!uint(cast(uint)indicies.length);
    fl.rawWriteExact(indicies[]);

    // line indicies
    fl.writeNum!uint(cast(uint)lindicies.length);
    fl.rawWriteExact(lindicies[]);
  }

  void load (VFile fl) {
    clear();

    char[8] sign = void;
    fl.rawReadExact(sign[]);
    if (sign[] != "K8VOXGL0") throw new Exception("invalid gl voxel data");

    const uint bidx = fl.readNum!uint;
    if (bidx != BreakIndex) throw new Exception("invalid break index");

    // texture
    imgWidth = fl.readNum!ushort;
    imgHeight = fl.readNum!ushort;
    delete img;
    img = new uint[imgWidth*imgHeight];
    fl.rawReadExact(img[]);

    // vertices
    uint tvcount = fl.readNum!uint;
    delete vertices;
    vertices = new VVoxVertexEx[tvcount];
    for (uint f = 0; f < tvcount; ++f) {
      vertices[f].x = fl.readNum!float;
      vertices[f].y = fl.readNum!float;
      vertices[f].z = fl.readNum!float;
      vertices[f].s = fl.readNum!float;
      vertices[f].t = fl.readNum!float;
      vertices[f].nx = fl.readNum!float;
      vertices[f].ny = fl.readNum!float;
      vertices[f].nz = fl.readNum!float;
    }

    // indicies
    uint indcount = fl.readNum!uint;
    delete indicies;
    indicies = new uint[indcount];
    fl.rawReadExact(indicies[]);

    // line indicies
    uint lindcount = fl.readNum!uint;
    delete lindicies;
    lindicies = new uint[lindcount];
    fl.rawReadExact(lindicies[]);

    sealed = true;
  }

public:
  uint appendVertex (in VVoxVertexEx gv) {
    assert(!sealed);
    if (auto vp = gv in vertcache) return *vp;
    ++uniqueVerts;
    immutable uint res = cast(uint)vertices.length;
    vertices ~= gv;
    vertcache[gv] = res;
    // fix min coords
    if (vmin[0] > gv.x) vmin[0] = gv.x;
    if (vmin[1] > gv.y) vmin[1] = gv.y;
    if (vmin[2] > gv.z) vmin[2] = gv.z;
    // fix max coords
    if (vmax[0] < gv.x) vmax[0] = gv.x;
    if (vmax[1] < gv.y) vmax[1] = gv.y;
    if (vmax[2] < gv.z) vmax[2] = gv.z;
    return res;
  }

private:
  enum {
    AXIS_X = 0,
    AXIS_Y = 1,
    AXIS_Z = 2,
  }

  /*
    here starts the t-junction fixing part
    probably the most complex piece of code here
    (because almost everything else is done elsewhere)

    the algorithm is very simple and fast, tho, because i can
    abuse the fact that vertices are always snapped onto the grid.
    so i simply created a bitmap that tells if there is any vertex
    at the given grid coords, and then walk over the edge, checking
    the bitmap, and adding the vertices. this is easy too, because
    all vertices are parallel to one of the coordinate axes. so no
    complex math required at all.

    another somewhat complex piece of code is triangle fan creator.
    there are four basic cases here.

    first: normal quad without any added vertices.
    we can simply copy its vertices, because such quad makes a valid fan.

    second: quad that have at least two edges without added vertices.
    we can use the shared vertex of those two edges as a starting point of
    a fan.

    third: two opposite edges have no added vertices.
    this can happen in "run conversion", and we can create two fans here.

    fourth: no above conditions are satisfied.
    this is the most complex case: to create a fan without degenerate triangles,
    we have to add a vertex in the center of the quad, and used it as a start of
    a triangle fan.

    note that we can always convert our triangle fans into triangle soup, so i
    didn't bothered to create a separate triangle soup code.
   */

  static struct VoxEdge {
    uint v0, v1; // start and end vertex
    float dir; // by the axis, not normalized
    float clo, chi; // low and high coords
    uint[] moreverts; // added vertices
    ubyte axis; // AXIS_n
  }


  VoxEdge[] edges;
  uint totaltadded;

  Vox3DBitmap gridbmp;
  int[3] gridmin;
  int[3] gridmax;


  void freeSortStructs () {
    gridbmp.clear();
  }

  void createGrid () {
    // create the grid
    for (uint f = 0; f < 3; ++f) {
      gridmin[f] = cast(int)vmin[f];
      gridmax[f] = cast(int)vmax[f];
    }
    uint gxs = cast(uint)(gridmax[0]-gridmin[0]+1);
    uint gys = cast(uint)(gridmax[1]-gridmin[1]+1);
    uint gzs = cast(uint)(gridmax[2]-gridmin[2]+1);
    conwriteln("vox dims: (", gridmin[0], ",", gridmin[1], ",", gridmin[2], ")-(",
               gridmax[0], ",", gridmax[1], ",", gridmax[2], ")");
    conwriteln("grid size: (", gxs, ",", gys, ",", gzs, ")");
    gridbmp.setSize(gxs, gys, gzs);
  }

  void gridCoords (float fx, float fy, float fz, int *gx, int *gy, int *gz) const nothrow @trusted @nogc {
    int vx = cast(int)fx;
    int vy = cast(int)fy;
    int vz = cast(int)fz;
    assert(vx >= gridmin[0] && vy >= gridmin[1] && vz >= gridmin[2]);
    assert(vx <= gridmax[0] && vy <= gridmax[1] && vz <= gridmax[2]);
    *gx = vx-gridmin[0];
    *gy = vy-gridmin[1];
    *gz = vz-gridmin[2];
  }

  void putVertexToGrid (uint vidx) {
    int vx, vy, vz;
    gridCoords(vertices[vidx].x, vertices[vidx].y, vertices[vidx].z, &vx, &vy, &vz);
    gridbmp.setPixel(vx, vy, vz);
  }

  uint hasVertexAt (float fx, float fy, float fz) const nothrow @trusted @nogc {
    int vx, vy, vz;
    gridCoords(fx, fy, fz, &vx, &vy, &vz);
    return gridbmp.getPixel(vx, vy, vz);
  }

  void putEdgeToGrid (uint eidx) {
    VoxEdge *e = &edges[eidx];
    putVertexToGrid(e.v0);
    putVertexToGrid(e.v1);
  }

  // create 3d grid, put edges into it
  void sortEdges () {
    createGrid();
    for (uint f = 0; f < cast(uint)edges.length; ++f) putEdgeToGrid(f);
  }

  // create list of edges
  void createEdges () {
    totaltadded = 0;
    edges.length = cast(uint)indicies.length/5U*4U; // one quad is 4 edges
    uint eidx = 0;
    uint uqcount = 0;
    for (uint f = 0; f < cast(uint)indicies.length; f += 5) {
      bool unitquad = true;
      for (uint vx0 = 0; vx0 < 4; ++vx0) {
        const uint vx1 = (vx0+1)&3;
        VoxEdge *e = &edges[eidx++];
        e.v0 = indicies[f+vx0];
        e.v1 = indicies[f+vx1];
        if (vertices[e.v0].x != vertices[e.v1].x) {
          assert(vertices[e.v0].y == vertices[e.v1].y);
          assert(vertices[e.v0].z == vertices[e.v1].z);
          e.axis = AXIS_X;
        } else if (vertices[e.v0].y != vertices[e.v1].y) {
          assert(vertices[e.v0].x == vertices[e.v1].x);
          assert(vertices[e.v0].z == vertices[e.v1].z);
          e.axis = AXIS_Y;
        } else {
          assert(vertices[e.v0].x == vertices[e.v1].x);
          assert(vertices[e.v0].y == vertices[e.v1].y);
          assert(vertices[e.v0].z != vertices[e.v1].z);
          e.axis = AXIS_Z;
        }
        e.clo = vertices[e.v0].get(e.axis);
        e.chi = vertices[e.v1].get(e.axis);
        e.dir = e.chi-e.clo;
        if (unitquad) unitquad = (e.dir == +1.0f || e.dir == -1.0f);
      }
      // "unit quads" can be ignored, they aren't interesting
      // also, each quad always have at least one "unit edge"
      // (this will be used to build triangle strips)
      uqcount += unitquad;
    }
    assert(eidx == cast(uint)edges.length);
    conwriteln(uqcount, " unit quad", (uqcount != 1 ? "s" : ""), " found.");
  }

  void fixEdgeWithVert (ref VoxEdge edge, float crd) {
    // calculate time
    const float tm = (crd-edge.clo)/edge.dir;

    const VVoxVertexEx *evx0 = &vertices[edge.v0];
    const VVoxVertexEx *evx1 = &vertices[edge.v1];

    VVoxVertexEx nvx = *evx0;
    // set coord
    nvx.set(edge.axis, crd);
    // calc new (s,t)
    nvx.s += (evx1.s-evx0.s)*tm;
    nvx.t += (evx1.t-evx0.t)*tm;

    edge.moreverts.assumeSafeAppend;
    edge.moreverts ~= appendVertex(nvx);
    ++totaltadded;
  }

  // fix one edge
  void fixEdgeNew (uint eidx) {
    VoxEdge *edge = &edges[eidx];
    if (edge.dir == +1.0f || edge.dir == -1.0f) return; // and here
    // check grid by the edge axis
    float[3] gxyz = void;
    for (uint f = 0; f < 3; ++f) gxyz[f] = vertices[edge.v0].get(f);
    const float step = (edge.dir < 0.0f ? -1.0f : +1.0f);
    gxyz[edge.axis] += step;
    while (gxyz[edge.axis] != edge.chi) {
      if (hasVertexAt(gxyz[0], gxyz[1], gxyz[2])) {
        fixEdgeWithVert(*edge, gxyz[edge.axis]);
      }
      gxyz[edge.axis] += step;
    }
  }

  void rebuildEdges () {
    // now we have to rebuild quads
    // each quad will have at least two unmodified edges of unit length
    uint newindcount = cast(uint)edges.length*5;
    for (uint f = 0; f < cast(uint)edges.length; ++f) {
      newindcount += cast(uint)edges[f].moreverts.length+8;
    }
    uint[] newind;
    newind.length = newindcount;

    newindcount = 0;
    for (uint f = 0; f < cast(uint)edges.length; f += 4) {
      // check if this quad is modified at all
      if (edges[f+0].moreverts.length ||
          edges[f+1].moreverts.length ||
          edges[f+2].moreverts.length ||
          edges[f+3].moreverts.length)
      {
        // this can be a quad that needs to be converted into a "centroid fan"
        // if we have at least two adjacent edges without extra points, we can use them
        // otherwise, we need to append a centroid
        int firstGoodFace = -1;
        for (uint c = 0; c < 4; ++c) {
          if (edges[f+c].moreverts.length == 0 &&
              edges[f+((c+1)&3)].moreverts.length == 0)
          {
            // i found her!
            firstGoodFace = cast(int)c;
            break;
          }
        }

        // have two good faces?
        if (firstGoodFace >= 0) {
          // yay, we can use v1 of the first face as the start of the fan
          assert(edges[f+firstGoodFace].moreverts.length == 0);
          newind[newindcount++] = edges[f+firstGoodFace].v1;
          // then v1 of the second good face
          firstGoodFace = (firstGoodFace+1)&3;
          assert(edges[f+firstGoodFace].moreverts.length == 0);
          newind[newindcount++] = edges[f+firstGoodFace].v1;
          // then add points of the other two faces (ignoring v0)
          for (uint c = 0; c < 2; ++c) {
            firstGoodFace = (firstGoodFace+1)&3;
            for (uint midx = 0; midx < edges[f+firstGoodFace].moreverts.length; ++midx) {
              newind[newindcount++] = edges[f+firstGoodFace].moreverts[midx];
            }
            newind[newindcount++] = edges[f+firstGoodFace].v1;
          }
          // we're done with this quad
          newind[newindcount++] = BreakIndex;
          continue;
        }

        // check if we have two opposite quads without extra points
        if ((edges[f+0].moreverts.length == 0 && edges[f+2].moreverts.length == 0) ||
            (edges[f+1].moreverts.length == 0 && edges[f+3].moreverts.length == 0) ||
            (edges[f+2].moreverts.length == 0 && edges[f+0].moreverts.length == 0) ||
            (edges[f+3].moreverts.length == 0 && edges[f+1].moreverts.length == 0))
        {
          // yes, we can use the algo for the strips here
          for (uint eic = 0; eic < 4; ++eic) {
            if (!edges[f+eic].moreverts.length) continue;
            const uint oic = (eic+3)&3; // previous edge
            // sanity checks
            assert(edges[f+oic].moreverts.length == 0);
            assert(edges[f+oic].v1 == edges[f+eic].v0);
            // create triangle fan
            newind[newindcount++] = edges[f+oic].v0;
            newind[newindcount++] = edges[f+eic].v0;
            // append additional vertices (they are already properly sorted)
            for (uint tmpf = 0; tmpf < cast(uint)edges[f+eic].moreverts.length; ++tmpf) {
              newind[newindcount++] = edges[f+eic].moreverts[tmpf];
            }
            // and the last vertex
            newind[newindcount++] = edges[f+eic].v1;
            // if the opposite side is not modified, we can finish the fan right now
            const uint loic = (eic+2)&3;
            if (edges[f+loic].moreverts.length == 0) {
              const uint noic = (eic+1)&3;
              // oic: prev
              // eic: current
              // noic: next
              // loic: last
              newind[newindcount++] = edges[f+noic].v1;
              newind[newindcount++] = BreakIndex;
              // we're done here
              break;
            }
            newind[newindcount++] = BreakIndex;
          }
          continue;
        }

        // alas, this quad should be converted to "centroid quad"
        // i.e. we will use quad center point to start a triangle fan

        // calculate quad center point
        float cx = 0.0f, cy = 0.0f, cz = 0.0f;
        float cs = 0.0f, ct = 0.0f;
        float prevx = 0.0f, prevy = 0.0f, prevz = 0.0f;
        bool xequal = true, yequal = true, zequal = true;
        for (uint eic = 0; eic < 4; ++eic) {
          cs += (vertices[edges[f+eic].v0].s+vertices[edges[f+eic].v1].s)*0.5f;
          ct += (vertices[edges[f+eic].v0].t+vertices[edges[f+eic].v1].t)*0.5f;
          const float vx = vertices[edges[f+eic].v0].x;
          const float vy = vertices[edges[f+eic].v0].y;
          const float vz = vertices[edges[f+eic].v0].z;
          cx += vx;
          cy += vy;
          cz += vz;
          if (eic) {
            xequal = xequal && (prevx == vx);
            yequal = yequal && (prevy == vy);
            zequal = zequal && (prevz == vz);
          }
          prevx = vx;
          prevy = vy;
          prevz = vz;
        }
        cx /= 4.0f;
        cy /= 4.0f;
        cz /= 4.0f;
        cs /= 4.0f;
        ct /= 4.0f;

        // determine quad orientation
        /*
        ubyte axis;
             if (xequal) axis = AXIS_X;
        else if (yequal) axis = AXIS_Y;
        else if (zequal) axis = AXIS_Z;
        else assert(0);
        */

        // calculate s and t
        //float s = vertices[edges[f+0].v0].s;
        //float t = vertices[edges[f+0].v0].t;
        float s = cs;
        float t = ct;

        // append center vertex
        VVoxVertexEx nvx = vertices[edges[f+0].v0];
        // set coord
        nvx.x = cx;
        nvx.y = cy;
        nvx.z = cz;
        // calc new (s,t)
        nvx.s = s;
        nvx.t = t;
        newind[newindcount++] = appendVertex(nvx);
        ++totaltadded;

        // append v0 of the first edge
        newind[newindcount++] = edges[f+0].v0;
        // append all vertices except v0 for all edges
        for (uint eic = 0; eic < 4; ++eic) {
          for (uint midx = 0; midx < edges[f+eic].moreverts.length; ++midx) {
            newind[newindcount++] = edges[f+eic].moreverts[midx];
          }
          newind[newindcount++] = edges[f+eic].v1;
        }
        newind[newindcount++] = BreakIndex;
      } else {
        // easy deal, just copy it
        newind[newindcount++] = edges[f+0].v0;
        newind[newindcount++] = edges[f+1].v0;
        newind[newindcount++] = edges[f+2].v0;
        newind[newindcount++] = edges[f+3].v0;
        newind[newindcount++] = BreakIndex;
      }
    }

    delete indicies;
    indicies = newind[0..newindcount];
    newind = null;
  }

  // t-junction fixer entry point
  // this will also convert vertex data to triangle strips
  void fixTJunctions () {
    const uint oldvtotal = cast(uint)vertices.length;
    createEdges();
    conwriteln(edges.length, " edges found (", edges.length/4*2, " tris, ", vertices.length, " verts)...");
    sortEdges();
    for (uint f = 0; f < cast(uint)edges.length; ++f) fixEdgeNew(f);
    freeSortStructs();
    if (totaltadded) {
      conwriteln(totaltadded, " t-fix vertices added (", vertices.length-oldvtotal, " unique).");
      rebuildEdges();
      conwriteln("rebuilt model: ", countTris(), " tris, ", vertices.length, " vertices.");
    }

    // cleanup
    foreach (ref VoxEdge ee; edges) {
      delete ee.moreverts; ee.moreverts = null;
    }
    delete edges; edges = null;
  }

public:
  bool isEmpty () const pure { pragma(inline, true); return (indicies.length == 0); }

  void clear () {
    delete vertices; vertices = null;
    delete indicies; indicies = null;
    delete lindicies; lindicies = null;

    vertcache.clear();
    uniqueVerts = 0;

    // our voxels are 1024x1024x1024 at max
    vmin[0] = vmin[1] = vmin[2] = +8192.0f;
    vmax[0] = vmax[1] = vmax[2] = -8192.0f;

    delete img; img = null;
    imgWidth = imgHeight = 0;

    glRelease();
    sealed = false;
  }


  // count the number of triangles in triangle fan data
  // used for informational messages
  uint countTris () {
    uint res = 0;
    uint ind = 0;
    while (ind < cast(uint)indicies.length) {
      assert(indicies[ind] != BreakIndex);
      uint end = ind+1;
      while (end < cast(uint)indicies.length && indicies[end] != BreakIndex) ++end;
      assert(end < indicies.length);
      assert(end-ind >= 3);
      if (end-ind == 3) {
        // simple triangle
        res += 1;
      } else if (end-ind == 4) {
        // quad
        res += 2;
      } else {
        // triangle fan
        res += end-ind-2;
      }
      ind = end+1;
    }
    return res;
  }


  // create lines for wireframe view
  void createLines () {
    lindicies.length = 0;
    lindicies.assumeSafeAppend;

    uint ind = 0;
    while (ind < cast(uint)indicies.length) {
      assert(indicies[ind] != BreakIndex);
      uint end = ind+1;
      while (end < cast(uint)indicies.length && indicies[end] != BreakIndex) ++end;
      assert(end < indicies.length);
      assert(end-ind >= 3);
      if (end-ind == 3) {
        // simple triangle
        lindicies ~= indicies[ind+0];
        lindicies ~= indicies[ind+1];
        lindicies ~= indicies[ind+2];
        lindicies ~= BreakIndex;
      } else if (end-ind == 4) {
        // quad
        lindicies ~= indicies[ind+0];
        lindicies ~= indicies[ind+1];
        lindicies ~= indicies[ind+2];
        lindicies ~= BreakIndex;

        lindicies ~= indicies[ind+2];
        lindicies ~= indicies[ind+3];
        lindicies ~= indicies[ind+0];
        lindicies ~= BreakIndex;
      } else {
        // triangle fan
        for (int f = ind+1; f < end-1; ++f) {
          lindicies ~= indicies[ind+0];
          lindicies ~= indicies[f+0];
          lindicies ~= indicies[f+1];
          lindicies ~= BreakIndex;
        }
      }
      ind = end+1;
    }
  }

  private static float calcTX (uint cp, int pos, uint sz) nothrow @safe @nogc {
    pragma(inline, true);
    float fp = cast(float)cast(int)cp;
         if (pos < 0) fp += 0.004f;
    else if (pos > 0) fp += 0.996f;
    else fp += 0.5f;
    return fp/cast(float)cast(int)sz;
  }

  // pos: <0 is left; 0 is center; >0 is right
  private float calcS (ref VoxelMesh vox, const ref VoxQuad vq, int pos) const nothrow @safe @nogc {
    uint cp = vox.catlas.getTexX(vq.cidx);
    if (pos > 0) cp += vq.wh.getW()-1;
    return calcTX(cp, pos, imgWidth);
  }

  private float calcT (ref VoxelMesh vox, const ref VoxQuad vq, int pos) const nothrow @safe @nogc {
    uint cp = vox.catlas.getTexY(vq.cidx);
    if (pos > 0) cp += vq.wh.getH()-1;
    return calcTX(cp, pos, imgHeight);
  }

  // main entry point
  void create (ref VoxelMesh vox) {
    clear();

    imgWidth = vox.catlas.getWidth();
    imgHeight = vox.catlas.getHeight();
    img = new uint[imgWidth*imgHeight];
    img[] = vox.catlas.colors[];

    version(none) {
      auto fo = VFile("zzz.raw", "w");
      fo.writeln(imgWidth, " ", imgHeight);
      foreach (uint dy; 0..imgHeight) {
        foreach (uint dx; 0..imgWidth) {
          if (dx) fo.write(" ");
          fo.writef("%08X", img[dy*imgWidth+dx]);
        }
        fo.writeln();
      }
    }

    auto tm = iv.timer.Timer(true);

    // calculate quad normals
    conwriteln("color texture size: ", imgWidth, "x", imgHeight);

    // create arrays
    foreach (ref VoxQuad vq; vox.quads[]) {
      uint[4] vxn = void;

      VVoxVertexEx gv = void;
      foreach (uint nidx; 0..4) {
        const VoxQuadVertex *vx = &vq.vx[nidx];
        gv.x = vx.x;
        gv.y = vx.y;
        gv.z = vx.z;
        if (vq.type == VoxelMesh.ZLong) {
          gv.s = calcS(vox, vq, (vx.dz ? 1 : -1));
          gv.t = calcT(vox, vq, 0);
        } else if (vq.type == VoxelMesh.XLong) {
          gv.s = calcS(vox, vq, (vx.dx ? 1 : -1));
          gv.t = calcT(vox, vq, 0);
        } else if (vq.type == VoxelMesh.YLong) {
          gv.s = calcS(vox, vq, (vx.dy ? 1 : -1));
          gv.t = calcT(vox, vq, 0);
        } else if (vq.type == VoxelMesh.Point) {
          gv.s = calcS(vox, vq, 0);
          gv.t = calcT(vox, vq, 0);
        } else {
          int spos = -1, tpos = -1;
          assert(vq.type == VoxelMesh.Quad);
          if (vq.cull&VoxelMesh.Cull_ZAxisMask) {
            if (vx.qtype&VoxelMesh.DMV_X) spos = 1;
            if (vx.qtype&VoxelMesh.DMV_Y) tpos = 1;
          } else if (vq.cull&VoxelMesh.Cull_XAxisMask) {
            if (vx.qtype&VoxelMesh.DMV_Y) spos = 1;
            if (vx.qtype&VoxelMesh.DMV_Z) tpos = 1;
          } else if (vq.cull&VoxelMesh.Cull_YAxisMask) {
            if (vx.qtype&VoxelMesh.DMV_X) spos = 1;
            if (vx.qtype&VoxelMesh.DMV_Z) tpos = 1;
          } else {
            assert(0);
          }
          gv.s = calcS(vox, vq, spos);
          gv.t = calcT(vox, vq, tpos);
        }
        gv.nx = vq.normal.x;
        gv.ny = vq.normal.y;
        gv.nz = vq.normal.z;
        vxn[nidx] = appendVertex(gv);
      }

      indicies ~= vxn[0];
      indicies ~= vxn[1];
      indicies ~= vxn[2];
      indicies ~= vxn[3];
      indicies ~= BreakIndex;
    }

    tm.stop();

    conwriteln(vox.quads.length, " quads, ", countTris(), " tris, ",
               uniqueVerts, " unique vertices (of ", vox.quads.length*4, ")");
    conwriteln("OpenGL data created in ", tm.toString());

    if (vox_fix_tjunctions) {
      tm.restart();
      fixTJunctions();
      tm.stop();
      conwriteln("t-junctions fixed in ", tm.toString());
      conwriteln("fixed tris: ", countTris());
    }

    createLines();
    vertcache.clear();
  }


  bool bufloaded = false;
  uint vvbo, ivbo, lvbo;
  uint ctxid; // color texture

  // release OpenGL resources
  void glRelease () {
    import iv.glbinds;
    if (!bufloaded) return;
    // vertex data
    glDeleteBuffers(1, &vvbo);
    // index data
    glDeleteBuffers(1, &ivbo);
    glDeleteBuffers(1, &lvbo);
    // texture
    glDeleteTextures(1, &ctxid);
    bufloaded = false;
  }

  void glUpload () {
    import iv.glbinds;
    if (bufloaded) return;

    glEnable(GL_PRIMITIVE_RESTART);
    glPrimitiveRestartIndex(BreakIndex);

    // vertex data
    glGenBuffers(1, &vvbo);
    glBindBuffer(GL_ARRAY_BUFFER, vvbo);
    glBufferData(GL_ARRAY_BUFFER, vertices[0].sizeof*vertices.length, vertices.ptr, GL_STATIC_DRAW);

    // index data
    glGenBuffers(1, &ivbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ivbo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicies[0].sizeof*indicies.length, indicies.ptr, GL_STATIC_DRAW);

    // line index data
    glGenBuffers(1, &lvbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, lvbo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicies[0].sizeof*lindicies.length, lindicies.ptr, GL_STATIC_DRAW);

    // texture
    glGenTextures(1, &ctxid);
    glBindTexture(GL_TEXTURE_2D, ctxid);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    GLfloat[4] bclr = 0.0;
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, bclr.ptr);
    // create straight from image
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imgWidth, imgHeight, 0, /*GL_RGBA*/GL_BGRA, GL_UNSIGNED_BYTE, img.ptr);

    bufloaded = true;
  }

  void draw () {
    import iv.glbinds;

    glUpload();

    //glColor3f(1.0f, 0.5f, 0.0f);
    //glColor3f(1.0f, 1.0f, 1.0f);
    glColor3f(1.0f, 1.0f, 1.0f);
    glBindBuffer(GL_ARRAY_BUFFER, vvbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (vox_wireframe ? lvbo : ivbo));
    glBindTexture(GL_TEXTURE_2D, ctxid);
    glEnable(GL_TEXTURE_2D);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    //glEnableClientState(GL_NORMAL_ARRAY);
    glVertexPointer(3, GL_FLOAT, vertices[0].sizeof, cast(void*)0);
    glTexCoordPointer(2, GL_FLOAT, vertices[0].sizeof, cast(void*)vertices[0].s.offsetof);
    if (vox_wireframe) {
      glDrawElements(GL_LINE_LOOP, cast(uint)lindicies.length, GL_UNSIGNED_INT, cast(void*)0);
    } else {
      glDrawElements(GL_TRIANGLE_FAN, cast(uint)indicies.length, GL_UNSIGNED_INT, cast(void*)0);
    }
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    //glDisableClientState(GL_NORMAL_ARRAY);
  }
};
