module vox_load;

import iv.bclamp;
import iv.glbinds.utils;
import iv.cmdcongl;
import iv.strex;
import iv.vfs;
import iv.vfs.io;
import iv.vmath;

static import iv.timer;

import vox_data;


// ////////////////////////////////////////////////////////////////////////// //
public __gshared bool vox_optimize_hollow = true;


//==========================================================================
//
//  loadKVX
//
//==========================================================================
private void loadKVX (ref VoxelData vox, VFile fl, uint fsize) {
  if (fsize > int.max/8) throw new Exception("voxel data too big (kvx)");
  version(kvx_dump) conwriteln("loading KVX...");
  auto nextpos = fl.tell+fsize;
  uint[] xofs;
  ushort[][] xyofs;
  ubyte[] data;
  int xsiz, ysiz, zsiz;
  int xpivot, ypivot, zpivot;
  {
    auto fx = wrapStreamRO(fl, fl.tell, fsize);
    xsiz = fx.readNum!uint;
    ysiz = fx.readNum!uint;
    zsiz = fx.readNum!uint;
    version(kvx_dump) conwriteln("xsiz=", xsiz, "; ysiz=", ysiz, "; zsiz=", zsiz);
    if (xsiz < 1 || ysiz < 1 || zsiz < 1 || xsiz > 1024 || ysiz > 1024 || zsiz > 1024) throw new Exception("invalid voxel size");
    xpivot = fx.readNum!uint;
    ypivot = fx.readNum!uint;
    zpivot = fx.readNum!uint;
    version(kvx_dump) conwriteln("xpivot=", xpivot>>8, "; ypivot=", ypivot>>8, "; zpivot=", zpivot>>8);
    uint xstart = (xsiz+1)*4+xsiz*(ysiz+1)*2;
    xofs = new uint[](xsiz+1);
    foreach (ref o; xofs) o = fx.readNum!uint-xstart;
    //conwriteln(xofs);
    xyofs = new ushort[][](xsiz, ysiz+1);
    foreach (immutable x; 0..xsiz) {
      foreach (immutable y; 0..ysiz+1) {
        xyofs[x][y] = fx.readNum!ushort;
      }
    }
    assert(fx.size-fx.tell == fsize-24-(xsiz+1)*4-xsiz*(ysiz+1)*2);
    data = new ubyte[](cast(uint)(fx.size-fx.tell));
    fx.rawReadExact(data);
  }
  fl.seek(nextpos);
  // skip mipmaps
  while (fl.size-fl.tell > 768) {
    fsize = fl.readNum!uint; // size of this voxel (useful for mipmaps)
    fl.seek(fsize, Seek.Cur);
    version(kvx_dump) conwriteln("  skiped ", fsize, " bytes of mipmap");
  }
  ubyte[768] pal;
  if (fl.size-fl.tell == 768) {
    version(kvx_dump) conwriteln("  has palette!");
    fl.rawReadExact(pal[]);
    foreach (ref ubyte c; pal[]) {
      if (c > 63) throw new Exception("invalid palette value");
      c = clampToByte(255*c/64);
    }
  } else {
    foreach (immutable cidx; 0..256) {
      pal[cidx*3+0] = cidx;
      pal[cidx*3+1] = cidx;
      pal[cidx*3+2] = cidx;
    }
  }
  immutable float px = 1.0f*xpivot/256.0f;
  immutable float py = 1.0f*ypivot/256.0f;
  immutable float pz = 1.0f*zpivot/256.0f;
  // now build cubes
  version(kvx_dump) conwriteln("building voxel data...");
  vox.setSize(xsiz, ysiz, zsiz);
  foreach (immutable y; 0..ysiz) {
    foreach (immutable x; 0..xsiz) {
      uint sofs = xofs[x]+xyofs[x][y];
      uint eofs = xofs[x]+xyofs[x][y+1];
      while (sofs < eofs) {
        int ztop = data[sofs++];
        uint zlen = data[sofs++];
        ubyte cull = data[sofs++];
        if (!zlen) continue;
        //conwritefln("  x=%s; y=%s; z=%s; len=%s; cull=0x%02x", x, y, ztop, zlen, cull);
        // colors
        foreach (immutable cidx; 0..zlen) {
          ubyte palcol = data[sofs++];
          ubyte cl = cull;
          version(none) {
            // no need to do this, voxel optimiser will take care of it
            if (cidx != 0) cl &= cast(ubyte)~0x10;
            if (cidx != zlen-1) cl &= cast(ubyte)~0x20;
          }
          immutable ubyte r = pal[palcol*3+0];
          immutable ubyte g = pal[palcol*3+1];
          immutable ubyte b = pal[palcol*3+2];
          //addCube(cl, (xsiz-x-1)-px, y-py, (zsiz-ztop-1)-pz, b|(g<<8)|(r<<16));
          //version(kvx_dump) conwriteln("  adding voxel at (", x, ",", y, ",", ztop, ")");
          vox.addVoxel(xsiz-x-1, y, zsiz-ztop-1, b|(g<<8)|(r<<16), cl);
          //version(kvx_dump) conwriteln("    added.");
          ++ztop;
        }
      }
      assert(sofs == eofs);
    }
  }
  vox.cx = px;
  vox.cy = py;
  vox.cz = pz;
}


//==========================================================================
//
//  loadKV6
//
//==========================================================================
private void loadKV6 (ref VoxelData vox, VFile fl) {
  version(kvx_dump) conwriteln("loading KV6...");
  int xsiz, ysiz, zsiz;
  float xpivot, ypivot, zpivot;
  xsiz = fl.readNum!uint;
  ysiz = fl.readNum!uint;
  zsiz = fl.readNum!uint;
  version(kvx_dump) conwriteln("xsiz=", xsiz, "; ysiz=", ysiz, "; zsiz=", zsiz);
  if (xsiz < 1 || ysiz < 1 || zsiz < 1 || xsiz > 1024 || ysiz > 1024 || zsiz > 1024) {
    throw new Exception("invalid voxel size (kv6)");
  }
  xpivot = fl.readNum!float;
  ypivot = fl.readNum!float;
  zpivot = fl.readNum!float;
  version(kvx_dump) conwriteln("xpivot=", xpivot, "; ypivot=", ypivot, "; zpivot=", zpivot);
  uint voxcount = fl.readNum!uint;
  if (voxcount == 0 || voxcount > int.max/8) throw new Exception("invalid number of voxels");
  static struct KVox {
    uint rgb;
    ubyte zlo;
    ubyte zhi;
    ubyte cull;
    ubyte normidx;
  }
  auto kvox = new KVox[](voxcount);
  foreach (immutable vidx; 0..voxcount) {
    auto kv = &kvox[vidx];
    kv.rgb = 0;
    kv.rgb |= fl.readNum!ubyte;
    kv.rgb |= fl.readNum!ubyte<<8;
    kv.rgb |= fl.readNum!ubyte<<16;
    kv.rgb |= 0xff_00_00_00U;
    fl.readNum!ubyte; // always 128; ignore
    kv.zlo = fl.readNum!ubyte;
    kv.zhi = fl.readNum!ubyte;
    if (kv.zhi) assert(0, "zhi is not zero!");
    kv.cull = fl.readNum!ubyte;
    kv.normidx = fl.readNum!ubyte;
  }
  auto xofs = new uint[](xsiz+1);
  uint curvidx = 0;
  foreach (ref v; xofs[0..$-1]) {
    v = curvidx;
    auto count = fl.readNum!uint;
    curvidx += count;
  }
  xofs[$-1] = curvidx;
  auto xyofs = new uint[][](xsiz, ysiz+1);
  foreach (immutable xx; 0..xsiz) {
    curvidx = 0;
    foreach (immutable yy; 0..ysiz) {
      xyofs[xx][yy] = curvidx;
      auto count = fl.readNum!ushort;
      curvidx += count;
    }
    xyofs[xx][$-1] = curvidx;
  }
  // now build cubes
  vox.setSize(xsiz, ysiz, zsiz);
  foreach (immutable y; 0..ysiz) {
    foreach (immutable x; 0..xsiz) {
      uint sofs = xofs[x]+xyofs[x][y];
      uint eofs = xofs[x]+xyofs[x][y+1];
      //if (sofs == eofs) continue;
      //assert(sofs < data.length && eofs <= data.length);
      while (sofs < eofs) {
        auto kv = &kvox[sofs++];
        //debug(kvx_dump) conwritefln("  x=%s; y=%s; zlo=%s; zhi=%s; cull=0x%02x", x, y, kv.zlo, kv.zhi, kv.cull);
        int z = kv.zlo;
        foreach (immutable cidx; 0..kv.zhi+1) {
          z += 1;
          vox.addVoxel(xsiz-x-1, y, zsiz-z, kv.rgb, kv.cull);
        }
      }
    }
  }
  //conwriteln(texpos);
  vox.cx = xpivot;
  vox.cy = ypivot;
  vox.cz = zpivot;
}


//==========================================================================
//
//  loadVox
//
//==========================================================================
private void loadVox (ref VoxelData vox, VFile fl, uint xsiz) {
  version(kvx_dump) conwriteln("loading VOX...");
  uint ysiz = fl.readNum!uint;
  uint zsiz = fl.readNum!uint;
  /*debug(kvx_dump)*/ conwriteln("xsiz=", xsiz, "; ysiz=", ysiz, "; zsiz=", zsiz);
  if (xsiz < 1 || ysiz < 1 || zsiz < 1 || xsiz > 1024 || ysiz > 1024 || zsiz > 1024) {
    throw new Exception("invalid voxel size (vox)");
  }
  auto fpos = fl.tell();
  fl.seek(fpos+xsiz*ysiz*zsiz);
  //auto data = new ubyte[](xsiz*ysiz*zsiz);
  //fl.rawReadExact(data);
  ubyte[768] pal;
  fl.rawReadExact(pal[]);
  foreach (ref ubyte c; pal[]) {
    if (c > 63) throw new Exception("invalid palette value");
    c = clampToByte(255*c/64);
  }
  immutable float px = 1.0f*xsiz/2.0f;
  immutable float py = 1.0f*ysiz/2.0f;
  immutable float pz = 1.0f*zsiz/2.0f;
  // now build cubes
  fl.seek(fpos);
  vox.setSize(xsiz, ysiz, zsiz);
  foreach (immutable x; 0..xsiz) {
    foreach (immutable y; 0..ysiz) {
      foreach (immutable z; 0..zsiz) {
        ubyte palcol = fl.readNum!ubyte;
        if (palcol != 255) {
          uint rgb = pal[palcol*3+2]|(cast(uint)pal[palcol*3+1]<<8)|(cast(uint)pal[palcol*3+0]<<16);
          //addCube(0xff, (xsiz-x-1)-px, y-py, (zsiz-z-1)-pz, b|(g<<8)|(r<<16));
          vox.addVoxel(xsiz-x-1, y, zsiz-z-1, rgb, 0xff);
        }
      }
    }
  }
  vox.cx = px;
  vox.cy = py;
  vox.cz = pz;
}


//==========================================================================
//
//  loadVxl
//
//==========================================================================
private void loadVxl (ref VoxelData vox, VFile fl) {
  uint xsiz = fl.readNum!uint;
  uint ysiz = fl.readNum!uint;
  uint zsiz = 256;
  /*debug(kvx_dump)*/ conwriteln("xsiz=", xsiz, "; ysiz=", ysiz, "; zsiz=", zsiz);
  if (xsiz < 1 || ysiz < 1 || zsiz < 1 || xsiz > 1024 || ysiz > 1024 || zsiz > 1024) throw new Exception("invalid voxel size");
  //immutable float px = 1.0f*xsiz/2.0f;
  //immutable float py = 1.0f*ysiz/2.0f;
  //immutable float pz = 1.0f*zsiz/2.0f;
  float px, py, pz;
  // camera
  px = fl.readNum!double;
  py = fl.readNum!double;
  pz = fl.readNum!double;
  pz = zsiz-1-pz;
  // unit right
  fl.readNum!double;
  fl.readNum!double;
  fl.readNum!double;
  // unit down
  fl.readNum!double;
  fl.readNum!double;
  fl.readNum!double;
  // unit forward
  fl.readNum!double;
  fl.readNum!double;
  fl.readNum!double;

  vox.setSize(xsiz, ysiz, zsiz);

  void vxlReset (int x, int y, int z) {
    vox.removeVoxel(xsiz-x-1, y, zsiz-z-1);
  }

  void vxlPaint (int x, int y, int z, uint clr) {
    vox.addVoxel(xsiz-x-1, y, zsiz-z-1, clr, 0x3f);
  }

  // now carve crap out of it
  auto data = new ubyte[](cast(uint)(fl.size-fl.tell));
  fl.rawReadExact(data);
  const(ubyte)* v = data.ptr;
  foreach (immutable y; 0..ysiz) {
    foreach (immutable x; 0..xsiz) {
      uint z = 0;
      for (;;) {
        foreach (immutable i; z..v[1]) vxlReset(x, y, i);
        for (z = v[1]; z <= v[2]; ++z) {
          const(uint)* cp = cast(const(uint)*)(v+(z-v[1]+1)*4);
          vxlPaint(x, y, z, *cp);
        }
        if (!v[0]) break;
        z = v[2]-v[1]-v[0]+2;
        v += v[0]*4;
        for (z += v[3]; z < v[3]; ++z) {
          const(uint)* cp = cast(const(uint)*)(v+(z-v[3])*4);
          vxlPaint(x, y, z, *cp);
        }
      }
      v += (v[2]-v[1]+2)*4;
    }
  }
  vox.cx = px;
  vox.cy = py;
  vox.cz = pz;
}


//==========================================================================
//
//  loadVoxel
//
//==========================================================================
public void loadVoxel (ref VoxelData vox, string fname, bool asvox=false) {
  auto fl = VFile(fname);
  uint fsize = fl.readNum!uint; // size of this voxel (useful for mipmaps)
  auto tm = iv.timer.Timer(true);
  if (fsize == 0x6c78764bU) {
    loadKV6(vox, fl);
  } else if (fsize == 0x09072000U) {
    loadVxl(vox, fl);
  } else {
    if (fname.endsWithCI(".vox")) loadVox(vox, fl, fsize);
    else loadKVX(vox, fl, fsize);
  }
  tm.stop();
  conwriteln("loaded in ", tm.toString(), "; grid size: ", vox.xsize, "x", vox.ysize, "x", vox.zsize);
  tm.restart();
  vox.optimise(vox_optimize_hollow);
  tm.stop();
  conwriteln("optimised in ", tm.toString());
}
