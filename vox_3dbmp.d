module vox_3dbmp;

import iv.bclamp;
import iv.glbinds.utils;
import iv.cmdcongl;
import iv.strex;
import iv.vfs;
import iv.vfs.io;
import iv.vmath;


// ////////////////////////////////////////////////////////////////////////// //
/*
  this is just a voxel cube, where each voxel represents by one bit.
  it is useful for fast "yes/no" queries and modifications, and is used
  in "hollow fill" and t-junction fixer algorithms.
 */
struct Vox3DBitmap {
  uint xsize = 0, ysize = 0, zsize = 0;
  uint xwdt = 0, xywdt = 0;
  uint[] bmp; // bit per voxel

public:
  void clear () {
    delete bmp; bmp = null;
    xsize = ysize = zsize = xwdt = xywdt = 0;
  }

  void setSize (uint xs, uint ys, uint zs) {
    clear();
    if (!xs || !ys || !zs) return;
    xsize = xs;
    ysize = ys;
    zsize = zs;
    xwdt = (xs+31)/32;
    assert(xwdt<<5 >= xs);
    xywdt = xwdt*ysize;
    bmp = new uint[xywdt*zsize];
    bmp[] = 0;
  }

  // returns old value
  uint setPixel (int x, int y, int z) nothrow @trusted @nogc {
    pragma(inline, true);
    if (x < 0 || y < 0 || z < 0) return 1;
    if (cast(uint)x >= xsize || cast(uint)y >= ysize || cast(uint)z >= zsize) return 1;
    uint *bp = bmp.ptr+cast(uint)z*xywdt+cast(uint)y*xwdt+(cast(uint)x>>5);
    const uint bmask = 1U<<(cast(uint)x&0x1f);
    const uint res = (*bp)&bmask;
    *bp |= bmask;
    return res;
  }

  void resetPixel (int x, int y, int z) nothrow @trusted @nogc {
    pragma(inline, true);
    if (x < 0 || y < 0 || z < 0) return;
    if (cast(uint)x >= xsize || cast(uint)y >= ysize || cast(uint)z >= zsize) return;
    bmp.ptr[cast(uint)z*xywdt+cast(uint)y*xwdt+(cast(uint)x>>5)] &= ~(1U<<(cast(uint)x&0x1f));
  }

  uint getPixel (int x, int y, int z) const nothrow @trusted @nogc {
    pragma(inline, true);
    if (x < 0 || y < 0 || z < 0) return 1;
    if (cast(uint)x >= xsize || cast(uint)y >= ysize || cast(uint)z >= zsize) return 1;
    return (bmp.ptr[cast(uint)z*xywdt+cast(uint)y*xwdt+(cast(uint)x>>5)]&(1U<<(cast(uint)x&0x1f)));
  }
}
