module vox_2dbmp;

import iv.bclamp;
import iv.glbinds.utils;
import iv.cmdcongl;
import iv.strex;
import iv.vfs;
import iv.vfs.io;
import iv.vmath;


// ////////////////////////////////////////////////////////////////////////// //
/*
  just a 2d bitmap that keeps rgb colors.
  there is also the code to find the biggest non-empty rectangle on the bitmap.
  empty pixels are represented with zeroes.

  the algorithm was taken from this SO topic: https://stackoverflow.com/questions/7245/
 */
struct Vox2DBitmap {
  static struct Pair {
    int one;
    int two;
  };

  int[] cache; // cache
  Pair[] stack; // stack
  int top; // top of stack

  void push (int a, int b) nothrow @safe @nogc {
    pragma(inline, true);
    stack[top].one = a;
    stack[top].two = b;
    ++top;
  }

  void pop (int *a, int *b) nothrow @safe @nogc {
    pragma(inline, true);
    --top;
    *a = stack[top].one;
    *b = stack[top].two;
  }


  int wdt, hgt; // dimension of input
  uint[] grid;
  uint[] ydotCount; // for each y coord
  int dotCount;


  void clear () {
    delete cache; cache = null;
    delete stack; stack = null;
    delete grid; grid = null;
    delete ydotCount; ydotCount = null;
    wdt = hgt = dotCount = 0;
  }


  void setSize (int awdt, int ahgt) {
    if (awdt < 1) awdt = 0;
    if (ahgt < 1) ahgt = 0;
    wdt = awdt;
    hgt = ahgt;
    if (grid.length < awdt*ahgt) grid.length = awdt*ahgt;
    grid[0..awdt*ahgt] = 0;
    if (ydotCount.length < ahgt) ydotCount.length = ahgt;
    ydotCount[0..hgt] = 0;
    dotCount = 0;
  }


  void clearBmp () {
    grid[0..wdt*hgt] = 0;
    ydotCount[0..hgt] = 0;
    dotCount = 0;
  }


  void setPixel (int x, int y, uint rgb) nothrow @trusted @nogc {
    pragma(inline, true);
    if (x < 0 || y < 0 || x >= wdt || y >= hgt) return;
    uint *cp = grid.ptr+cast(uint)y*cast(uint)wdt+cast(uint)x;
    if (!*cp) {
      ++dotCount;
      ++ydotCount.ptr[y];
    }
    *cp = rgb;
  }

  uint resetPixel (int x, int y) nothrow @trusted @nogc {
    pragma(inline, true);
    if (x < 0 || y < 0 || x >= wdt || y >= hgt) return 0;
    uint *cp = grid.ptr+cast(uint)y*cast(uint)wdt+cast(uint)x;
    const uint res = *cp;
    if (res) {
      *cp = 0;
      --dotCount;
      --ydotCount.ptr[y];
    }
    return res;
  }


  void updateCache (int currY) nothrow @trusted @nogc {
    /*
    for (int m = 0; m < wdt; ++m, ++cp) {
      if (!grid[currY*wdt+m]) cache[m] = 0; else ++cache[m];
    }
    */
    const(uint) *lp = grid.ptr+cast(uint)currY*cast(uint)wdt;
    int *cp = cache.ptr;
    for (int m = wdt; m--; ++cp) {
      if (*lp++) ++(*cp); else *cp = 0;
    }
  }


  /*
    this is the slowest part of the conversion code.
   */
  bool doOne (int *rx0, int *ry0, int *rx1, int *ry1) nothrow @trusted {
    if (dotCount == 0) return false;

    if (cache.length < wdt+1) cache.length = wdt+1;
    if (stack.length < wdt+1) stack.length = wdt+1;

    Pair best_ll;
    Pair best_ur;
    int best_area;

    best_ll.one = best_ll.two = 0;
    best_ur.one = best_ur.two = -1;
    best_area = 0;
    top = 0;
    bool cacheCleared = true;

    for (int m = 0; m <= wdt; ++m) cache[m] = stack[m].one = stack[m].two = 0;

    // main algorithm
    for (int n = 0; n < hgt; ++n) {
      // there is no need to scan empty lines
      // (and we usually have quite a lot of them)
      if (ydotCount[n] == 0) {
        if (!cacheCleared) {
          cacheCleared = true;
          cache[0..wdt] = 0;
        }
        continue;
      }
      int openWidth = 0;
      updateCache(n);
      cacheCleared = false;
      const(int) *cp = cache.ptr;
      for (int m = 0; m <= wdt; ++m) {
        const int cvl = *cp++;
        if (cvl > openWidth) {
          // open new rectangle
          push(m, openWidth);
          openWidth = cvl;
        } else if (cvl < openWidth) {
          // close rectangle(s)
          int m0 = void, w0 = void;
          do {
            pop(&m0, &w0);
            const int area = openWidth*(m-m0);
            if (area > best_area) {
              best_area = area;
              best_ll.one = m0; best_ll.two = n;
              best_ur.one = m-1; best_ur.two = n-openWidth+1;
            }
            openWidth = w0;
          } while (cvl < openWidth);
          openWidth = cvl;
          if (openWidth != 0) push(m0, w0);
        }
      }
    }

    *rx0 = (best_ll.one < best_ur.one ? best_ll.one : best_ur.one);
    *ry0 = (best_ll.two < best_ur.two ? best_ll.two : best_ur.two);
    *rx1 = (best_ll.one > best_ur.one ? best_ll.one : best_ur.one);
    *ry1 = (best_ll.two > best_ur.two ? best_ll.two : best_ur.two);

    // remove dots
    /*
    for (int y = *ry0; y <= *ry1; ++y) {
      for (int x = *rx0; x <= *rx1; ++x) {
        resetPixel(x, y);
      }
    }
    */

    return true;
  }
};
