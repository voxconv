module zglvox_new is aliced;

import arsd.simpledisplay;
//import arsd.color;
//import arsd.image;

import iv.bclamp;
import iv.glbinds.utils;
import iv.cmdcongl;
import iv.strex;
import iv.vfs;
import iv.vfs.io;
import iv.vmath;

static import iv.timer;

import vox_texatlas;
import vox_2dbmp;
import vox_3dbmp;
import vox_data;
import vox_load;
import vox_mesh;
import vox_meshgl;

//version = kvx_dump;
//version = vox_check_invariants;
version = voxdata_debug;


// ////////////////////////////////////////////////////////////////////////// //
__gshared int scrWidth = 800, scrHeight = 800;
__gshared bool paused = true;


// ////////////////////////////////////////////////////////////////////////// //
shared static this () {
  conRegVar!scrWidth(64, 4096, "v_width", "screen width");
  conRegVar!scrHeight(64, 4096, "v_height", "screen height");
}


public void renderSealVars () {
  conSealVar("v_width");
  conSealVar("v_height");
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared VoxelData voxdata;
__gshared VoxelMesh vox;
__gshared GLVoxelMesh glvox;
__gshared bool glcreated = false;
__gshared int angle = 360/3;
__gshared bool vertical = false;
__gshared vec3 eye = vec3(0.0f, 14.0f, 64.0f);


void drawScene () {
  import iv.glbinds;

  if (!glcreated) return;

  glDisable(GL_TEXTURE_2D);
  glEnable(GL_DEPTH_TEST);
  glDisable(GL_LIGHTING);
  glDisable(GL_DITHER);
  glEnable(GL_COLOR_MATERIAL);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glDepthFunc(GL_LEQUAL);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

  glEnable(GL_CULL_FACE);
  //glCullFace(GL_FRONT);
  glCullFace(GL_BACK);
  //glFrontFace(GL_CCW);
  glFrontFace(GL_CW);

  //glDisable(GL_CULL_FACE); // this way we can draw any model
  glShadeModel(GL_FLAT);

  //glDisable(GL_CULL_FACE); // this way we can draw any model

  glDisable(GL_BLEND);

  glClearDepth(1.0f);
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

  // setup projection
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  oglPerspective(
    90.0, // field of view in degree
    1.0, // aspect ratio
    1.0, // Z near
    10000.0, // Z far
  );
  version(none) {
    oglLookAt(
      0.0, -vox.cy-64, 30.0, // eye is at (0,0,5)
      0.0, 0.0, 0.0, // center is at (0,0,0)
      0.0, 0.0, 1.0, // up is in positive Z direction
    );
  } else {
    oglLookAt(
      eye.x, eye.y-vox.cy-64, eye.z, // eye is at (0,0,5)
      0.0, 0.0, 0.0, // center is at (0,0,0)
      0.0, 0.0, 1.0, // up is in positive Z direction
    );
  }

  // setup model matrix
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  //glRotatef(1.0f*angle, 0.8f, 0.9f, 0.4f);
  //glRotatef(1.0f*angle, 1.0f, 0.0f, 0.0f);
  //glRotatef(1.0f*angle, 0.0f, 1.0f, 0.0f);
  if (vertical) {
    glRotatef(1.0f*angle, 1.0f, 0.0f, 0.0f);
  } else {
    glRotatef(1.0f*angle, 0.0f, 0.0f, 1.0f);
  }

  glvox.draw();
}


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  //setOpenGLContextVersion(3, 2); // up to GLSL 150
  //openGLContextCompatible = false;

  if (args.length < 1) {
    writeln("filename?");
    return;
  }

  version(none) {
    loadVoxel(ref voxdata, args[2]);
    return;
  }

  conRegVar!paused("pause", "stop rotating");
  conRegVar!angle(0, 359, "angle", "current angle");
  conRegVar!vertical("vertical", "rotate vertically");

  conProcessArgs(args);
  while (conProcessQueue()) {}

  renderSealVars();

  int argidx = 1;

  auto sdwin = new SimpleWindow(conGetVar!int("v_width"), conGetVar!int("v_height"),
                                "KVX Renderer",
                                OpenGlOptions.yes, Resizability.fixedSize);


  void loadFrom (string fname) {
    writeln("loading: ", fname, (vox_allow_normals ? " (with normals)" : " (no normals)"));
    loadVoxel(ref voxdata, fname);
    vox.createFrom(ref voxdata);
    voxdata.clear();
    glvox.create(vox);
    glcreated = true;
    import std.string : format;
    sdwin.title = "(%s quads, %s verts) -- %s".format(vox.quads.length, glvox.uniqueVerts, fname);
    vox.clear();
    { import core.memory : GC; GC.collect(); GC.minimize(); }
    //{ import core.memory : GC; GC.collect(); GC.minimize(); }
  }


  sdwin.redrawOpenGlScene = delegate () {
    drawScene();
    glconDraw();
  };

  sdwin.visibleForTheFirstTime = delegate () {
    loadFrom(args[argidx]);
    //vox = new Voxel(VFile("untitled.vxl"));
    sdwin.setAsCurrentOpenGlContext(); // make this window active
    glconResize(sdwin.width, sdwin.height);
    sdwin.redrawOpenGlScene();
    //drawScene();
  };

  sdwin.eventLoop(1000/35,
    delegate () {
      if (sdwin.closed) return;
      conProcessQueue();
      if (isQuitRequested) { sdwin.close(); return; }
      if (!paused) angle = (angle+1)%360;
      //angle = 50;
      //angle = 360/2;
      sdwin.redrawOpenGlSceneNow();
    },
    delegate (KeyEvent event) {
      if (sdwin.closed) return;
      if (glconKeyEvent(event)) return;
      if (!event.pressed) return;
      if (event.pressed && event.key == Key.Escape) { concmd("quit"); return; }
      /*
      switch (event.key) {
        case Key.Escape: sdwin.close(); break;
        default:
      }
      */
      if (event.pressed) {
        float delta = 10.0f;
        if (event.modifierState&ModifierState.shift) {
          delta = (event.modifierState&ModifierState.ctrl ? 5.0f : 1.0f);
        } else if (event.modifierState&ModifierState.ctrl) {
          delta = 20.0f;
        }
        if (event.modifierState&ModifierState.alt) {
          if (event.key == Key.Up) eye.y -= delta;
          if (event.key == Key.Down) eye.y += delta;
        } else {
          if (event.key == Key.Left) eye.x -= delta;
          if (event.key == Key.Right) eye.x += delta;
          if (event.key == Key.Up) eye.z -= delta;
          if (event.key == Key.Down) eye.z += delta;
        }
      }
    },
    delegate (MouseEvent event) {
    },
    delegate (dchar ch) {
      if (sdwin.closed) return;
      if (glconCharEvent(ch)) return;
      if (ch == 'q') { concmd("quit"); return; }
      if (ch == ' ') { concmd("pause toggle"); return; }
      if (ch == 'v') { concmd("vertical toggle"); return; }
      if (ch == '0') { eye = vec3(0.0f, 0.0f, 0.0f); return; }
      if (ch == 'u') { vox_wireframe = !vox_wireframe; return; }
      if (ch == 't') {
        vox_fix_tjunctions = !vox_fix_tjunctions;
        loadFrom(args[argidx]);
        return;
      }
      if (ch == 'l') {
        vox_allow_normals = !vox_allow_normals;
        loadFrom(args[argidx]);
        return;
      }
      if (ch == 'h') {
        vox_optimize_hollow = !vox_optimize_hollow;
        loadFrom(args[argidx]);
        return;
      }
      if (ch == '1' || ch == '2' || ch == '3' || ch == '4' || ch == '5') {
        vox_optimisation = (ch-'1');
        loadFrom(args[argidx]);
        return;
      }
      if (ch == 'O') {
        vox_optimiser_use_3dbmp = !vox_optimiser_use_3dbmp;
        conwriteln("OPTIMISER: use3dbmp is ", (vox_optimiser_use_3dbmp ? "on" : "off"));
        loadFrom(args[argidx]);
        return;
      }
      if (ch == 'C') {
        vox_atlas_cache_colors = !vox_atlas_cache_colors;
        conwriteln("OPTIMISER: cache_colors is ", (vox_atlas_cache_colors ? "on" : "off"));
        loadFrom(args[argidx]);
        return;
      }
      if (ch == 'F') {
        vox_optimiser_use_cvox = !vox_optimiser_use_cvox;
        conwriteln("OPTIMISER: use faster voxel data is ", (vox_optimiser_use_cvox ? "on" : "off"));
        loadFrom(args[argidx]);
        return;
      }
      if (ch == 'p' || ch == 'P') {
        if (argidx > 1) {
          --argidx;
          loadFrom(args[argidx]);
        }
        return;
      }
      if (ch == 'n' || ch == 'N') {
        if (argidx+1 < args.length) {
          ++argidx;
          loadFrom(args[argidx]);
        }
        return;
      }
    },
  );
  flushGui();
}
