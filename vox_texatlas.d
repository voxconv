module vox_texatlas;

import iv.bclamp;
import iv.glbinds.utils;
import iv.cmdcongl;
import iv.strex;
import iv.vfs;
import iv.vfs.io;
import iv.vmath;


// ////////////////////////////////////////////////////////////////////////// //
public __gshared bool vox_atlas_cache_colors = true;


// ////////////////////////////////////////////////////////////////////////// //
struct VoxTexAtlas {
public:
  static struct Rect {
    int x, y, w, h;
    pure nothrow @safe @nogc {
      static Rect Invalid () { pragma(inline, true); return Rect.init; }
      bool isValid () const { pragma(inline, true); return (x >= 0 && y >= 0 && w > 0 && h > 0); }
      int getArea () const { pragma(inline, true); return w*h; }
    }
  }

private:
  int imgWidth, imgHeight;
  Rect[] rects;

  enum BadRect = uint.max;

public:
  void clear () {
    delete rects; rects = null;
    imgWidth = imgHeight = 0;
  }

  void setSize (int awdt, int ahgt) {
    clear();
    assert(awdt > 0 && ahgt > 0);
    imgWidth = awdt;
    imgHeight = ahgt;
    rects.assumeSafeAppend;
    rects ~= Rect(0, 0, awdt, ahgt); // one big rect
  }

  @property const pure nothrow @safe @nogc {
    int width () { pragma(inline, true); return imgWidth; }
    int height () { pragma(inline, true); return imgHeight; }
  }

  void dumpRects () const nothrow @trusted @nogc {
    import core.stdc.stdio : fprintf, stderr;
    fprintf(stderr, "=== ATLAS: %ux%u (%u rects) ===\n", imgWidth, imgHeight, cast(uint)rects.length);
    for (uint f = 0; f < cast(uint)rects.length; ++f) {
      fprintf(stderr, "  %4u: (%d,%d)-(%d,%d)\n", rects[f].x, rects[f].y,
              rects[f].x+rects[f].w-1, rects[f].y+rects[f].h-1);
    }
    fprintf(stderr, "-----------\n");
  }

  // node id or BadRect
  private uint findBestFit (int w, int h) nothrow @trusted @nogc {
    uint fitW = BadRect, fitH = BadRect, biggest = BadRect;

    foreach (immutable idx, const ref r; rects) {
      if (r.w < w || r.h < h) continue; // absolutely can't fit
      if (r.w == w && r.h == h) return cast(uint)idx; // perfect fit
      if (r.w == w) {
        // width fit
        if (fitW == BadRect || rects.ptr[fitW].h < r.h) fitW = cast(uint)idx;
      } else if (r.h == h) {
        // height fit
        if (fitH == BadRect || rects.ptr[fitH].w < r.w) fitH = cast(uint)idx;
      } else {
        // get biggest rect
        if (biggest == BadRect || rects.ptr[biggest].getArea() > r.getArea()) biggest = cast(uint)idx;
      }
    }
    // both?
    if (fitW != BadRect && fitH != BadRect) return (rects.ptr[fitW].getArea() > rects.ptr[fitH].getArea() ? fitW : fitH);
    if (fitW != BadRect) return fitW;
    if (fitH != BadRect) return fitH;
    return biggest;
  }

  // returns invalid rect if there's no room
  Rect insert (int cwdt, int chgt/*, const(uint)[] colors*/) {
    assert(cwdt > 0 && chgt > 0);
    auto ri = findBestFit(cwdt, chgt);
    if (ri == BadRect) return Rect.Invalid;
    auto rc = rects.ptr[ri];
    auto res = Rect(rc.x, rc.y, cwdt, chgt);
    // split this rect
    if (rc.w == res.w && rc.h == res.h) {
      // best fit, simply remove this rect
      foreach (immutable cidx; ri+1..rects.length) rects.ptr[cidx-1] = rects.ptr[cidx];
      rects.length -= 1;
      rects.assumeSafeAppend; // for future; we probably won't have alot of best-fitting nodes initially
    } else {
      if (rc.w == res.w) {
        // split vertically
        rc.y += res.h;
        rc.h -= res.h;
      } else if (rc.h == res.h) {
        // split horizontally
        rc.x += res.w;
        rc.w -= res.w;
      } else {
        Rect nr = rc;
        // split in both directions (by longer edge)
        if (rc.w-res.w > rc.h-res.h) {
          // cut the right part
          nr.x += res.w;
          nr.w -= res.w;
          // cut the bottom part
          rc.y += res.h;
          rc.h -= res.h;
          rc.w = res.w;
        } else {
          // cut the bottom part
          nr.y += res.h;
          nr.h -= res.h;
          // cut the right part
          rc.x += res.w;
          rc.w -= res.w;
          rc.h = res.h;
        }
        rects.assumeSafeAppend;
        rects ~= nr;
      }
      rects.ptr[ri] = rc;
    }
    return res;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// just a compact representation of a rectange
// splitted to two copy-pasted structs for better type checking
align(1) struct VoxXY16 {
align(1):
  uint xy; // low word: x; high word: y

  nothrow @safe @nogc {
    this (uint x, uint y) { pragma(inline, true); xy = (y<<16)|(x&0xffffU); }
    uint getX () const pure { pragma(inline, true); return (xy&0xffffU); }
    uint getY () const pure { pragma(inline, true); return (xy>>16); }
    void setX (uint x) { pragma(inline, true); xy = (xy&0xffff0000U)|(x&0xffffU); }
    void setY (uint y) { pragma(inline, true); xy = (xy&0x0000ffffU)|(y<<16); }
  }
}


align(1) struct VoxWH16 {
align(1):
  uint wh; // low word: x; high word: y

  nothrow @safe @nogc {
    this (uint w, uint h) { pragma(inline, true); wh = (h<<16)|(w&0xffffU); }
    uint getW () const pure { pragma(inline, true); return (wh&0xffffU); }
    uint getH () const pure { pragma(inline, true); return (wh>>16); }
    void setW (uint w) { pragma(inline, true); wh = (wh&0xffff0000U)|(w&0xffffU); }
    void setH (uint h) { pragma(inline, true); wh = (wh&0x0000ffffU)|(h<<16); }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// color atlas, ready to be uploaded to the GPU
struct VoxColorPack {
public:
  static struct ColorItem {
    VoxXY16 xy; // start position
    VoxWH16 wh; // size
    VoxXY16 newxy; // used in relayouter
    int next; // -1: no more
  }

public:
  uint clrwdt, clrhgt;
  uint[] colors; // clrwdt by clrhgt

  ColorItem[] citems;
  int[uint] citemhash; // key: color index; value: index in `citems`

  VoxTexAtlas atlas;

public:
  uint getWidth () const pure nothrow @safe @nogc { pragma(inline, true); return clrwdt; }
  uint getHeight () const pure nothrow @safe @nogc { pragma(inline, true); return clrhgt; }

  uint getTexX (uint cidx) const pure nothrow @safe @nogc {
    pragma(inline, true);
    return citems[cidx].xy.getX();
  }

  uint getTexY (uint cidx) const pure nothrow @safe @nogc {
    pragma(inline, true);
    return citems[cidx].xy.getY();
  }


  void clear () {
    delete colors; colors = null;
    delete citems; citems = null;
    citemhash.clear();
    atlas.clear();
    clrwdt = clrhgt = 0;
  }


  // prepare for new run
  void reset () {
    clear();
  }


  // grow image, and relayout everything
  void growImage (uint inswdt, uint inshgt) {
    uint neww = clrwdt, newh = clrhgt;
    while (neww < inswdt) neww <<= 1;
    while (newh < inshgt) newh <<= 1;
    for (;;) {
      if (neww < newh) neww <<= 1; else newh <<= 1;
      // relayout data
      bool again = false;
      atlas.setSize(neww, newh);
      for (int f = 0; f < cast(int)citems.length; ++f) {
        ColorItem *ci = &citems[f];
        auto rc = atlas.insert(cast(int)ci.wh.getW(), cast(int)ci.wh.getH());
        if (!rc.isValid()) {
          // alas, no room
          again = true;
          break;
        }
        // record new coords
        ci.newxy = VoxXY16(rc.x, rc.y);
      }
      if (!again) break; // done
    }

    // allocate new image, copy old data
    conwriteln("ATLAS: resized from ", clrwdt, "x", clrhgt, " to ", neww, "x", newh);
    uint[] newclr = new uint[neww*newh];
    newclr[] = 0;
    for (int f = 0; f < cast(int)citems.length; ++f) {
      ColorItem *ci = &citems[f];
      const uint rcw = ci.wh.getW();
      uint oaddr = ci.xy.getY()*clrwdt+ci.xy.getX();
      uint naddr = ci.newxy.getY()*neww+ci.newxy.getX();
      uint dy = ci.wh.getH();
      /*
      conwriteln(": : : oldpos=(", ci.rc.getX(), ",", ci.rc.getY(), "); newpos=(", newx, ",",
                 newy, "); size=(", rcw, "x", ci.rc.getH(), "); oaddr=", oaddr, "; naddr=", naddr);
      */
      while (dy--) {
        newclr[naddr..naddr+rcw] = colors[oaddr..oaddr+rcw];
        oaddr += clrwdt;
        naddr += neww;
      }
      ci.xy = ci.newxy;
    }
    delete colors;
    colors = newclr;
    clrwdt = neww;
    clrhgt = newh;
    newclr = null;
  }


  // returns true if found, and sets `*cidxp` and `*xyofsp`
  // `*xyofsp` is offset inside `cidxp`
  bool findRectEx (const(uint)[] clrs, uint cwdt, uint chgt, uint cxofs, uint cyofs,
                   uint wdt, uint hgt, uint *cidxp, VoxWH16 *whp)
  {
    assert(wdt > 0 && hgt > 0);
    assert(cwdt >= wdt && chgt >= hgt);

    const uint saddrOrig = cyofs*cwdt+cxofs;
    auto cp = clrs[saddrOrig] in citemhash;
    if (!cp) return false;

    for (int cidx = *cp; cidx >= 0; cidx = citems[cidx].next) {
      const ColorItem *ci = &citems[cidx];
      if (wdt > ci.wh.getW() || hgt > ci.wh.getH()) continue; // impossibiru
      // compare colors
      bool ok = true;
      uint saddr = saddrOrig;
      uint caddr = ci.xy.getY()*clrwdt+ci.xy.getX();
      for (uint dy = 0; dy < hgt; ++dy) {
        if (colors[caddr..caddr+wdt] != clrs[saddr..saddr+wdt]) {
          ok = false;
          break;
        }
        saddr += cwdt;
        caddr += clrwdt;
      }
      if (ok) {
        // i found her!
        // topmost
        if (cidxp !is null) *cidxp = cast(uint)cidx;
        if (whp !is null) *whp = VoxWH16(wdt, hgt);
        return true;
      }
    }

    return false;
  }


  bool findRect (const(uint)[] clrs, uint wdt, uint hgt, uint *cidxp, VoxWH16 *whp) {
    pragma(inline, true);
    return (vox_atlas_cache_colors ? findRectEx(clrs, wdt, hgt, 0, 0, wdt, hgt, cidxp, whp) : false);
  }


  // returns index in `citems`
  uint addNewRect (const(uint)[] clrs, uint wdt, uint hgt) {
    assert(wdt > 0 && hgt > 0);
    VoxXY16 coord;

    if (clrwdt == 0) {
      // no rects yet
      assert(clrhgt == 0);
      clrwdt = 1;
      while (clrwdt < wdt) clrwdt <<= 1;
      clrhgt = 1;
      while (clrhgt < hgt) clrhgt <<= 1;
      if (clrhgt < clrwdt) clrhgt = clrwdt; //!!
      //clrwdt = clrhgt = 1024;
      atlas.setSize(clrwdt, clrhgt);
      colors.length = clrwdt*clrhgt;
      colors[] = 0;
    }

    // insert into atlas; grow texture if cannot insert
    //atlas.dumpRects();
    for (;;) {
      auto rc = atlas.insert(cast(int)wdt, cast(int)hgt);
      if (rc.isValid()) {
        coord = VoxXY16(rc.x, rc.y);
        break;
      }
      // no room, grow the texture, and relayout everything
      growImage(wdt, hgt);
    }

    /*
    atlas.dumpRects();
    conwriteln("**ATLAS: coord=(", coord.getX(), ",", coord.getY(), ")-(",
               coord.getX()+wdt-1, "x", coord.getY()+hgt-1, ")");
    */

    // copy source colors into the atlas image
    uint saddr = 0;
    uint daddr = coord.getY()*clrwdt+coord.getX();
    for (uint dy = 0; dy < hgt; ++dy) {
      foreach (uint b; colors[daddr..daddr+wdt]) assert(b == 0);
      colors[daddr..daddr+wdt] = clrs[saddr..saddr+wdt];
      saddr += wdt;
      daddr += clrwdt;
    }

    // hash main rect
    ColorItem ci;
    ci.xy = coord;
    ci.wh = VoxWH16(wdt, hgt);
    const int parentIdx = cast(int)citems.length;
    if (vox_atlas_cache_colors) {
      uint cc = clrs[0];
      auto cpp = cc in citemhash;
      if (cpp) {
        ci.next = *cpp;
        *cpp = parentIdx;
      } else {
        ci.next = -1;
        citemhash[cc] = parentIdx;
      }
    }
    citems.assumeSafeAppend;
    citems ~= ci;

    return cast(uint)parentIdx;
  }
}
