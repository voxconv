module vox_mesh;

import iv.bclamp;
import iv.glbinds.utils;
import iv.cmdcongl;
import iv.strex;
import iv.vfs;
import iv.vfs.io;
import iv.vmath;

static import iv.timer;
import iv.pxclock;

import vox_texatlas;
import vox_2dbmp;
import vox_3dbmp;
import vox_data;


// ////////////////////////////////////////////////////////////////////////// //
public enum kvx_max_optim_level = 4;
public __gshared int vox_optimisation = kvx_max_optim_level-1;
public __gshared bool vox_allow_normals = true;
// this helps a little, so why not
public __gshared bool vox_optimiser_use_3dbmp = true;
public __gshared bool vox_optimiser_use_cvox = true;


// ////////////////////////////////////////////////////////////////////////// //
static align(1) struct VoxQuadVertex {
align(1):
  float x, y, z;
  float dx, dy, dz;
  ubyte qtype; // Xn_Yn_Zn
}

// quad is always one texel strip
static align(1) struct VoxQuad {
  VoxQuadVertex[4] vx;
  uint cidx; // in colorpack's `citems`
  VoxWH16 wh; // width and height
  VoxQuadVertex normal;
  int type = -1/*Invalid*/;
  ubyte cull; // for which face this quad was created?
}


/*
  this thing is used to create a quad mesh from a voxel data.
  it contains several conversion methods, from the most simple one
  "one quad for each visible voxel face", to the most complex one,
  that goes through each voxel plane, and joins individual voxel
  faces into a bigger quads (i.e. quads covering several faces at once).

  this doesn't directly calculates texture coords, tho: it is using an
  atlas, and records rect coords for each quad. real texture coords are
  calculated in GL mesh builder instead.
*/
struct VoxelMesh {
  // quad type
  enum {
    Invalid = -1,
    Point,
    XLong,
    YLong,
    ZLong,
    Quad,
  }

  enum {
    Cull_Right  = 0x01, // x axis
    Cull_Left   = 0x02, // x axis
    Cull_Near   = 0x04, // y axis
    Cull_Far    = 0x08, // y axis
    Cull_Top    = 0x10, // z axis
    Cull_Bottom = 0x20, // z axis

    Cull_XAxisMask = (Cull_Right|Cull_Left),
    Cull_YAxisMask = (Cull_Near|Cull_Far),
    Cull_ZAxisMask = (Cull_Top|Cull_Bottom),
  }

  enum {
    DMV_X = 0b100,
    DMV_Y = 0b010,
    DMV_Z = 0b001,
  }

  // bitmasks, `DMV_n` can be used to check for `0` or `1`
  enum {
    X0_Y0_Z0,
    X0_Y0_Z1,
    X0_Y1_Z0,
    X0_Y1_Z1,
    X1_Y0_Z0,
    X1_Y0_Z1,
    X1_Y1_Z0,
    X1_Y1_Z1,
  }

  VoxQuad[] quads;
  // voxel center point
  float cx, cy, cz;
  // color atlas
  VoxColorPack catlas;

  static immutable ubyte[4][6] quadFaces = [
    // right (&0x01) (right)
    [
      X1_Y1_Z0,
      X1_Y0_Z0,
      X1_Y0_Z1,
      X1_Y1_Z1,
    ],
    // left (&0x02) (left)
    [
      X0_Y0_Z0,
      X0_Y1_Z0,
      X0_Y1_Z1,
      X0_Y0_Z1,
    ],
    // top (&0x04) (near)
    [
      X0_Y0_Z0,
      X0_Y0_Z1,
      X1_Y0_Z1,
      X1_Y0_Z0,
    ],
    // bottom (&0x08) (far)
    [
      X1_Y1_Z0,
      X1_Y1_Z1,
      X0_Y1_Z1,
      X0_Y1_Z0,
    ],
    // back (&0x10)  (top)
    [
      X0_Y1_Z1,
      X1_Y1_Z1,
      X1_Y0_Z1,
      X0_Y0_Z1,
    ],
    // front (&0x20)  (bottom)
    [
      X0_Y0_Z0,
      X1_Y0_Z0,
      X1_Y1_Z0,
      X0_Y1_Z0,
    ]
  ];

  static immutable float[3][6] quadNormals = [
    // right (&0x01) (right)
    [ 1.0f, 0.0f, 0.0f],
    // left (&0x02) (left)
    [-1.0f, 0.0f, 0.0f],
    // top (&0x04) (near)
    [ 0.0f,-1.0f, 0.0f],
    // bottom (&0x08) (far)
    [ 0.0f, 1.0f, 0.0f],
    // back (&0x10)  (top)
    [ 0.0f, 0.0f, 1.0f],
    // front (&0x20)  (bottom)
    [ 0.0f, 0.0f,-1.0f],
  ];

  private void quadCalcNormal (ref VoxQuad vq) {
    if (vox_allow_normals) {
      uint qidx;
      switch (vq.cull) {
        case 0x01: qidx = 0; break;
        case 0x02: qidx = 1; break;
        case 0x04: qidx = 2; break;
        case 0x08: qidx = 3; break;
        case 0x10: qidx = 4; break;
        case 0x20: qidx = 5; break;
        default: assert(0);
      }
      vq.normal.x = vq.normal.dx = quadNormals[qidx][0];
      vq.normal.y = vq.normal.dy = quadNormals[qidx][1];
      vq.normal.z = vq.normal.dz = quadNormals[qidx][2];
    } else {
      vq.normal.x = vq.normal.dx = 0.0f;
      vq.normal.y = vq.normal.dy = 0.0f;
      vq.normal.z = vq.normal.dz = 1.0f;
    }
  }

  private void setColors (ref VoxQuad vq, const(uint)[] clrs, uint wdt, uint hgt) {
    if (catlas.findRect(clrs, wdt, hgt, &vq.cidx, &vq.wh)) {
      /*
      conwriteln("  ...reused rect: (", catlas.getTexX(vq.cidx, vq.rc), ",",
                 catlas.getTexY(vq.cidx, vq.rc), ")-(", wdt, "x", hgt, ")");
      */
      return;
    }
    vq.cidx = catlas.addNewRect(clrs, wdt, hgt);
    vq.wh = VoxWH16(wdt, hgt);
  }

  private static VoxQuadVertex genVertex (ubyte type, const float x, const float y, const float z,
                                   const float xlen, const float ylen, const float zlen)
  {
    VoxQuadVertex vx = void;
    vx.qtype = type;
    vx.dx = vx.dy = vx.dz = 0.0f;
    vx.x = x;
    vx.y = y;
    vx.z = z;
    if (type&DMV_X) { vx.x += xlen; vx.dx = 1.0f; }
    if (type&DMV_Y) { vx.y += ylen; vx.dy = 1.0f; }
    if (type&DMV_Z) { vx.z += zlen; vx.dz = 1.0f; }
    return vx;
  }

  // dmv: bit 2 means XLong, bit 1 means YLong, bit 0 means ZLong
  void addSlabFace (ubyte cull, ubyte dmv,
                    float x, float y, float z,
                    int len, const(uint)[] colors)
  {
    if (len < 1) return;
    assert(dmv == DMV_X || dmv == DMV_Y || dmv == DMV_Z);
    assert(cull == 0x01 || cull == 0x02 || cull == 0x04 || cull == 0x08 || cull == 0x10 || cull == 0x20);
    assert(colors.length >= len);
    colors = colors[0..len];

    bool allsame = true;
    foreach (auto cidx; 1..colors.length) {
      if (colors[cidx] != colors[0]) {
        allsame = false;
        break;
      }
    }
    if (allsame) colors = colors[0..1];

    const int qtype =
      colors.length == 1 ? Point :
      (dmv&DMV_X) ? XLong :
      (dmv&DMV_Y) ? YLong :
      ZLong;
    const float dx = (dmv&DMV_X ? cast(float)len : 1.0f);
    const float dy = (dmv&DMV_Y ? cast(float)len : 1.0f);
    const float dz = (dmv&DMV_Z ? cast(float)len : 1.0f);
    uint qidx;
    switch (cull) {
      case 0x01: qidx = 0; break;
      case 0x02: qidx = 1; break;
      case 0x04: qidx = 2; break;
      case 0x08: qidx = 3; break;
      case 0x10: qidx = 4; break;
      case 0x20: qidx = 5; break;
      default: assert(0);
    }
    VoxQuad vq;
    foreach (uint vidx; 0..4) {
      vq.vx[vidx] = genVertex(quadFaces[qidx][vidx], x, y, z, dx, dy, dz);
    }
    setColors(ref vq, colors[], cast(uint)colors.length, 1);
    vq.type = qtype;
    vq.cull = cull;
    quadCalcNormal(vq);
    quads ~= vq;
  }

  void addCube (ubyte cull, float x, float y, float z, uint rgb) {
    immutable uint[1] carr = [rgb];
    // generate quads
    foreach (uint qidx; 0..6) {
      const ubyte cmask = VoxelData.cullmask(qidx);
      if (cull&cmask) {
        addSlabFace(cmask, DMV_X/*doesn't matter*/, x, y, z, 1, carr[]);
      }
    }
  }

  void addQuad (ubyte cull,
                float x, float y, float z,
                int wdt, int hgt, // quad size
                const(uint)[] colors)
  {
    assert(wdt > 0 && hgt > 0);
    assert(cull == 0x01 || cull == 0x02 || cull == 0x04 || cull == 0x08 || cull == 0x10 || cull == 0x20);
    assert(colors.length >= wdt*hgt);
    colors = colors[0..wdt*hgt];

    bool allsame = true;
    foreach (auto cidx; 1..colors.length) {
      if (colors[cidx] != colors[0]) {
        allsame = false;
        break;
      }
    }
    if (allsame) colors = colors[0..1];

    const int qtype = Quad;
    uint qidx;
    switch (cull) {
      case 0x01: qidx = 0; break;
      case 0x02: qidx = 1; break;
      case 0x04: qidx = 2; break;
      case 0x08: qidx = 3; break;
      case 0x10: qidx = 4; break;
      case 0x20: qidx = 5; break;
      default: assert(0);
    }

    VoxQuad vq;
    for (uint vidx = 0; vidx < 4; ++vidx) {
      const ubyte vtype = quadFaces[qidx][vidx];
      VoxQuadVertex vx = void;
      vx.qtype = vtype;
      vx.dx = vx.dy = vx.dz = 0.0f;
      vx.x = x;
      vx.y = y;
      vx.z = z;
      if (cull&Cull_ZAxisMask) {
        if (vtype&DMV_X) vx.dx = cast(float)wdt;
        if (vtype&DMV_Y) vx.dy = cast(float)hgt;
        if (vtype&DMV_Z) vx.dz = 1.0f;
      } else if (cull&Cull_XAxisMask) {
        if (vtype&DMV_X) vx.dx = 1.0f;
        if (vtype&DMV_Y) vx.dy = cast(float)wdt;
        if (vtype&DMV_Z) vx.dz = cast(float)hgt;
      } else if (cull&Cull_YAxisMask) {
        if (vtype&DMV_X) vx.dx = cast(float)wdt;
        if (vtype&DMV_Y) vx.dy = 1.0f;
        if (vtype&DMV_Z) vx.dz = cast(float)hgt;
      } else {
        assert(0);
      }
      vx.x += vx.dx;
      vx.y += vx.dy;
      vx.z += vx.dz;
      vq.vx[vidx] = vx;
    }

    if (colors.length == 1) {
      setColors(ref vq, colors[], 1, 1);
    } else {
      setColors(ref vq, colors[], wdt, hgt);
    }

    vq.type = qtype;
    vq.cull = cull;
    quadCalcNormal(vq);
    quads ~= vq;
  }


  void buildOpt0 (ref VoxelData vox) {
    const float px = vox.cx;
    const float py = vox.cy;
    const float pz = vox.cz;
    foreach (int y; 0..vox.ysize) {
      foreach (int x; 0..vox.xsize) {
        uint dofs = vox.getDOfs(x, y);
        while (dofs) {
          addCube(vox.data[dofs].cull, x-px, y-py, vox.data[dofs].z-pz, vox.data[dofs].rgb());
          dofs = vox.data[dofs].nextz;
        }
      }
    }
  }

  void buildOpt1 (ref VoxelData vox) {
    const float px = vox.cx;
    const float py = vox.cy;
    const float pz = vox.cz;

    uint[1024] slab = void;

    foreach (int y; 0..vox.ysize) {
      foreach (int x; 0..vox.xsize) {
        // try slabs in all 6 directions?
        uint dofs = vox.getDOfs(x, y);
        if (!dofs) continue;

        // long top and bottom quads
        while (dofs) {
          foreach (uint cidx; 4..6) {
            const ubyte cmask = VoxelData.cullmask(cidx);
            if ((vox.data[dofs].cull&cmask) == 0) continue;
            const int z = cast(int)vox.data[dofs].z;
            slab[0] = vox.data[dofs].rgb();
            addSlabFace(cmask, DMV_X, x-px, y-py, z-pz, 1, slab[0..1]);
          }
          dofs = vox.data[dofs].nextz;
        }

        // build long quads for each side
        foreach (uint cidx; 0..4) {
          const ubyte cmask = VoxelData.cullmask(cidx);
          dofs = vox.getDOfs(x, y);
          while (dofs) {
            while (dofs && (vox.data[dofs].cull&cmask) == 0) dofs = vox.data[dofs].nextz;
            if (!dofs) break;
            const int z = cast(int)vox.data[dofs].z;
            int count = 0;
            uint eofs = dofs;
            while (eofs && (vox.data[eofs].cull&cmask)) {
              if (cast(int)vox.data[eofs].z != z+count) break;
              vox.data[eofs].cull ^= cmask;
              slab[count] = vox.data[eofs].rgb();
              eofs = vox.data[eofs].nextz;
              ++count;
              if (count == cast(int)slab.length) break;
            }
            assert(count);
            dofs = eofs;
            addSlabFace(cmask, DMV_Z, x-px, y-py, z-pz, count, slab[0..count]);
          }
        }
      }
    }
  }

  void buildOpt2 (ref VoxelData vox) {
    const float px = vox.cx;
    const float py = vox.cy;
    const float pz = vox.cz;

    uint[1024] slab = void;

    foreach (int y; 0..vox.ysize) {
      foreach (int x; 0..vox.xsize) {
        // try slabs in all 6 directions?
        uint dofs = vox.getDOfs(x, y);
        if (!dofs) continue;

        // long top and bottom quads
        while (dofs) {
          foreach (uint cidx; 4..6) {
            const ubyte cmask = VoxelData.cullmask(cidx);
            if ((vox.data[dofs].cull&cmask) == 0) continue;
            const int z = cast(int)vox.data[dofs].z;
            assert(vox.queryCull(x, y, z) == vox.data[dofs].cull);
            // by x
            int xcount = 0;
            while (x+xcount < cast(int)vox.xsize) {
              const ubyte vcull = vox.queryCull(x+xcount, y, z);
              if ((vcull&cmask) == 0) break;
              ++xcount;
            }
            // by y
            int ycount = 0;
            while (y+ycount < cast(int)vox.ysize) {
              const ubyte vcull = vox.queryCull(x, y+ycount, z);
              if ((vcull&cmask) == 0) break;
              ++ycount;
            }
            assert(xcount && ycount);
            // now use the longest one
            if (xcount >= ycount) {
              xcount = 0;
              while (x+xcount < cast(int)vox.xsize) {
                const uint vrgb = vox.query(x+xcount, y, z);
                if (((vrgb>>24)&cmask) == 0) break;
                slab[xcount] = vrgb|0xff000000U;
                vox.setVoxelCull(x+xcount, y, z, (vrgb>>24)^cmask);
                ++xcount;
              }
              assert(xcount);
              addSlabFace(cmask, DMV_X, x-px, y-py, z-pz, xcount, slab[0..xcount]);
            } else {
              ycount = 0;
              while (y+ycount < cast(int)vox.ysize) {
                const uint vrgb = vox.query(x, y+ycount, z);
                if (((vrgb>>24)&cmask) == 0) break;
                slab[ycount] = vrgb|0xff000000U;
                vox.setVoxelCull(x, y+ycount, z, (vrgb>>24)^cmask);
                ++ycount;
              }
              assert(ycount);
              addSlabFace(cmask, DMV_Y, x-px, y-py, z-pz, ycount, slab[0..ycount]);
            }
          }
          dofs = vox.data[dofs].nextz;
        }

        // build long quads for each side
        foreach (uint cidx; 0..4) {
          const ubyte cmask = VoxelData.cullmask(cidx);
          dofs = vox.getDOfs(x, y);
          while (dofs) {
            while (dofs && (vox.data[dofs].cull&cmask) == 0) dofs = vox.data[dofs].nextz;
            if (!dofs) break;
            const int z = cast(int)vox.data[dofs].z;
            int count = 0;
            uint eofs = dofs;
            while (eofs && (vox.data[eofs].cull&cmask)) {
              if (cast(int)vox.data[eofs].z != z+count) break;
              vox.data[eofs].cull ^= cmask;
              slab[count] = vox.data[eofs].rgb();
              eofs = vox.data[eofs].nextz;
              ++count;
              if (count == cast(int)slab.length) break;
            }
            assert(count);
            dofs = eofs;
            addSlabFace(cmask, DMV_Z, x-px, y-py, z-pz, count, slab[0..count]);
          }
        }
      }
    }
  }

  void buildOpt3 (ref VoxelData vox) {
    const float px = vox.cx;
    const float py = vox.cy;
    const float pz = vox.cz;

    // try slabs in all 6 directions?
    uint[1024] slab = void;

    immutable ubyte[2][3] dmove = [
      [DMV_Y, DMV_Z], // left, right
      [DMV_X, DMV_Z], // near, far
      [DMV_X, DMV_Y], // top, bottom
    ];

    static int getDX (in ubyte dmv) pure nothrow @safe @nogc { pragma(inline, true); return !!(dmv&DMV_X); }
    static int getDY (in ubyte dmv) pure nothrow @safe @nogc { pragma(inline, true); return !!(dmv&DMV_Y); }
    static int getDZ (in ubyte dmv) pure nothrow @safe @nogc { pragma(inline, true); return !!(dmv&DMV_Z); }

    static void incXYZ (in ubyte dmv, ref int sx, ref int sy, ref int sz) nothrow @safe @nogc {
      pragma(inline, true);
      sx += getDX(dmv);
      sy += getDY(dmv);
      sz += getDZ(dmv);
    }

    foreach (int y; 0..vox.ysize) {
      foreach (int x; 0..vox.xsize) {
        for (uint dofs = vox.getDOfs(x, y); dofs; dofs = vox.data[dofs].nextz) {
          while (vox.data[dofs].cull) {
            uint count = 0;
            ubyte clrdmv = 0;
            ubyte clrmask = 0;
            const int z = cast(int)vox.data[dofs].z;
            // check all faces
            foreach (uint cidx; 0..6) {
              const ubyte cmask = VoxelData.cullmask(cidx);
              if ((vox.data[dofs].cull&cmask) == 0) continue;
              // try two dirs
              foreach (uint ndir; 0..2) {
                const ubyte dmv = dmove[cidx>>1][ndir];
                int cnt = 1;
                int sx = x, sy = y, sz = z;
                incXYZ(dmv, ref sx, ref sy, ref sz);
                for (;;) {
                  const ubyte vxc = vox.queryCull(sx, sy, sz);
                  if ((vxc&cmask) == 0) break;
                  ++cnt;
                  incXYZ(dmv, ref sx, ref sy, ref sz);
                }
                if (cnt > count) {
                  count = cnt;
                  clrdmv = dmv;
                  clrmask = cmask;
                }
              }
            }
            if (clrmask) {
              assert(count);
              assert(clrdmv == DMV_X || clrdmv == DMV_Y || clrdmv == DMV_Z);
              int sx = x, sy = y, sz = z;
              for (uint f = 0; f < count; ++f) {
                VoxPix *vp = vox.queryVP(sx, sy, sz);
                slab[f] = vp.rgb();
                assert(vp.cull&clrmask);
                vp.cull ^= clrmask;
                incXYZ(clrdmv, ref sx, ref sy, ref sz);
              }
              addSlabFace(clrmask, clrdmv, x-px, y-py, z-pz, count, slab[0..count]);
            }
          }
        }
      }
    }
  }


  // 128 chars should be enough for everyone
  static char[] milliToBuffer (uint msecs, char[] dest) nothrow @trusted @nogc {
    import core.stdc.stdio : snprintf;
    char[128] buf = void;
    //immutable uint micro = 0; msecs /= 1000;
    immutable uint milli = cast(uint)(msecs%1000);
    msecs /= 1000;
    immutable uint seconds = cast(uint)(msecs%60);
    msecs /= 60;
    immutable uint minutes = cast(uint)(msecs%60);
    msecs /= 60;
    immutable uint hours = cast(uint)msecs;
    uint len;
         if (hours) len = cast(uint)snprintf(buf.ptr, buf.length, "%u:%02u:%02u.%03u", hours, minutes, seconds, milli);
    else if (minutes) len = cast(uint)snprintf(buf.ptr, buf.length, "%u:%02u.%03u", minutes, seconds, milli);
    else if (seconds) len = cast(uint)snprintf(buf.ptr, buf.length, "%u.%03u", seconds, milli);
    else if (milli >= 10) len = cast(uint)snprintf(buf.ptr, buf.length, "%ums", milli);
    //else if (micro != 0) len = cast(uint)snprintf(buf.ptr, buf.length, "%ums:%umcs", milli, micro);
    else len = cast(uint)snprintf(buf.ptr, buf.length, "%ums", milli);
    if (len > dest.length) len = cast(uint)dest.length;
    dest.ptr[0..len] = buf.ptr[0..len];
    return dest.ptr[0..len];
  }

  // this tries to create big quads
  void buildOpt4 (ref VoxelData vox) {
    const float px = vox.cx;
    const float py = vox.cy;
    const float pz = vox.cz;

    // try slabs in all 6 directions?
    uint[] slab = null;
    scope(exit) { delete slab; slab = null; }

    // for faster scans
    Vox3DBitmap bmp3d;
    scope(exit) bmp3d.clear();
    if (vox_optimiser_use_3dbmp) vox.create3DBitmap(ref bmp3d);

    VoxelDataSmall vxopt;
    scope(exit) vxopt.clear();
    if (vox_optimiser_use_cvox) vxopt.createFrom(ref vox);

    const ulong tstart = clockMilli();
    ulong tlastreport = 0;
    auto tm = iv.timer.Timer(true);

    char[129] tbuf = void;
    char[129] tbuf2 = void;
    bool wasPrint = false;

    Vox2DBitmap bmp2d;
    for (uint cidx = 0; cidx < 6; ++cidx) {
      const ubyte cmask = VoxelData.cullmask(cidx);

      uint vwdt, vhgt, vlen;
      if (cmask&Cull_ZAxisMask) {
        vwdt = vox.xsize;
        vhgt = vox.ysize;
        vlen = vox.zsize;
      } else if (cmask&Cull_XAxisMask) {
        vwdt = vox.ysize;
        vhgt = vox.zsize;
        vlen = vox.xsize;
      } else {
        vwdt = vox.xsize;
        vhgt = vox.zsize;
        vlen = vox.ysize;
      }
      bmp2d.setSize(vwdt, vhgt);

      for (uint vcrd = 0; vcrd < vlen; ++vcrd) {
        //bmp2d.clearBmp(); // no need to, it is guaranteed
        assert(bmp2d.dotCount == 0);
        for (uint vdy = 0; vdy < vhgt; ++vdy) {
          for (uint vdx = 0; vdx < vwdt; ++vdx) {
            uint vx, vy, vz;
                 if (cmask&Cull_ZAxisMask) { vx = vdx; vy = vdy; vz = vcrd; }
            else if (cmask&Cull_XAxisMask) { vx = vcrd; vy = vdx; vz = vdy; }
            else { vx = vdx; vy = vcrd; vz = vdy; }
            //conwriteln("*vcrd=", vcrd, "; vdx=", vdx, "; vdy=", vdy);
            if (vox_optimiser_use_3dbmp && !bmp3d.getPixel(vx, vy, vz)) continue;
            if (vox_optimiser_use_cvox) {
              const uint vd = vxopt.queryVox(vx, vy, vz);
              if (((vd>>24)&cmask) == 0) continue;
              bmp2d.setPixel(vdx, vdy, vd|0xff000000U);
            } else {
              auto vp = vox.queryVP(vx, vy, vz);
              if (!vp || ((*vp).cull&cmask) == 0) continue;
              bmp2d.setPixel(vdx, vdy, (*vp).rgb());
              if (((*vp).cull ^= cmask) == 0) {
                vox.removeVoxel(vx, vy, vz);
                if (vox_optimiser_use_3dbmp) bmp3d.resetPixel(vx, vy, vz);
              }
            }
          }
        }
        //conwriteln(":: cidx=", cidx, "; vcrd=", vcrd, "; dotCount=", bmp2d.dotCount);
        if (bmp2d.dotCount == 0) continue;
        // ok, we have some dots, go create quads
        int x0, y0, x1, y1;
        while (bmp2d.doOne(&x0, &y0, &x1, &y1)) {
          const uint cwdt = (x1-x0)+1;
          const uint chgt = (y1-y0)+1;
          if (slab.length < cwdt*chgt) slab.length = ((cwdt*chgt)|0xff)+1;
          // get colors
          uint *dp = slab.ptr;
          for (int dy = y0; dy <= y1; ++dy) {
            for (int dx = x0; dx <= x1; ++dx) {
              *dp++ = bmp2d.resetPixel(dx, dy);
            }
          }
          float fx, fy, fz;
               if (cmask&Cull_ZAxisMask) { fx = x0; fy = y0; fz = vcrd; }
          else if (cmask&Cull_XAxisMask) { fx = vcrd; fy = x0; fz = y0; }
          else { fx = x0; fy = vcrd; fz = y0; }
          addQuad(cmask, fx-px, fy-py, fz-pz, cwdt, chgt, slab[0..cwdt*chgt]);
        }
        {
          const ulong ctt = clockMilli();
          if (ctt-tlastreport >= 1000) {
            tlastreport = ctt;
            const uint curr = vlen*cidx+vcrd+1U;
            const uint total = vlen*6U;
            const uint stt = cast(uint)(ctt-tstart);
            const uint ett = cast(uint)(cast(ulong)stt*total/curr);
            tbuf[] = 0;
            tbuf2[] = 0;
            char[] bf = milliToBuffer(stt, tbuf[0..$-1]);
            char[] bf2 = milliToBuffer(ett, tbuf2[0..$-1]);
            import core.stdc.stdio : fprintf, stdout, fflush;
            fprintf(stdout, " %3u%%  %s (ETA: %s)\x1b[K\r", 100U*curr/total, bf.ptr, bf2.ptr);
            fflush(stdout);
            wasPrint = true;
          }
        }
      }
    }
    tm.stop();
    if (wasPrint) {
      import core.stdc.stdio : fprintf, stdout, fflush;
      fprintf(stdout, "\r\x1b[K"); fflush(stdout);
    }
    conwriteln("*** ultra conversion done in ", tm.toString());
  }


  void createFrom (ref VoxelData vox) {
    assert(vox.xsize && vox.ysize && vox.zsize);
    // now build cubes
    conwriteln("building slabs...");
    auto tm = iv.timer.Timer(true);
    switch (vox_optimisation) {
      case 0: buildOpt0(vox); break;
      case 1: buildOpt1(vox); break;
      case 2: buildOpt2(vox); break;
      case 3: buildOpt3(vox); break;
      case 4: default: buildOpt4(vox); break;
    }
    tm.stop();
    conwriteln("basic conversion: ", quads.length, " quads (", quads.length*2, " tris).");
    conwriteln("converted in ", tm.toString(), "; optlevel is ", vox_optimisation, "/", kvx_max_optim_level);
    cx = vox.cx;
    cy = vox.cy;
    cz = vox.cz;
  }

  void clear () {
    delete quads; quads = null;
    catlas.clear();
    cx = cy = cz = 0.0f;
  }

  bool isEmpty () const pure { pragma(inline, true); return (quads.length == 0); }
}
